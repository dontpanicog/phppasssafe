<?php
/**
 * Popup Fenster mit dem neue Gruppen angelegt werden
 */

require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/PasswordGroups.php';
require_once 'model/Groups.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;


if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die('you are not logged in');
}

$error          = '';
$close_window   = 'OnLoad="document.add_group.group_name.focus()"';
$filtered_input = array();

if (isset($_POST['action'])) {
	if (isset($_POST['group_name'])){
		$allowed_group_name_characters = ALLOWED_CHARACTERS;
		if (preg_match($allowed_group_name_characters, $_POST['group_name'])){
			$filtered_input['group_name'] = $_POST['group_name'];
		} else {
			$error .= ILLEGAL_GROUPNAME . '<br>';
		}
	} else {
		$error .= MISSING_GROUPNAME . '<br>';
	}
	if ($error == '') {
		$added_group = new Groups;
		$added_group->GroupName  = $filtered_input['group_name'];
		$added_group->add_group($_COOKIE['phpPassSafe']);

        // jetzt noch eine gleichnamige Password Group anlegen
        $group_id = $added_group->fetch_group_id_for_group_name($filtered_input['group_name']);

        $added_group = new PasswordGroups;
        $added_group->PasswordGroupName  = $filtered_input['group_name'];
        $added_group->UserGroupID        = $group_id;
        $added_group->private            = 0;
        $added_group->add_group($_COOKIE['phpPassSafe']);

		$close_window = 'OnLoad="window.close()"';
	}
}

$smarty = new Smarty;
$smarty->assign('error', $error);
$smarty->assign('close_window', $close_window);
$smarty->assign('passwordgroup', PASSWORDGROUP);
$smarty->assign('usergroup', USERGROUP);
$smarty->assign('ok', OK);
$smarty->assign('cancel', CANCEL);
$smarty->assign('delete', DELETE);
$smarty->display('add_group.html');
?>
