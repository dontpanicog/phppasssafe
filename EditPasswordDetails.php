<?php
/*
 * Popup Fenster zum Anlegen und editieren von Passwoertern
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/UserGroup_PasswordGroup_Mapping.php';
require_once 'model/Passwords.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;


$logger = new Logger;
$logger->debug('EditPasswordDetails was called');
$show_updated_password = false;

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
	die('you are not logged in');
}

$error            = '';
$filtered_input   = array();
$function_to_load = '';

if (isset($_GET['password_id'])) {
	if (ctype_digit($_GET['password_id'])) {
		$filtered_input['password_id'] = $_GET['password_id'];
	}
}

if (isset($_GET['preselct_group_id'])) {
	if (ctype_digit($_GET['preselct_group_id'])) {
		$filtered_input['preselct_group_id'] = $_GET['preselct_group_id'];
	}
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'OK') {
		if (ctype_digit($_POST['password_id'])){
			$filtered_input['password_id'] = $_POST['password_id'];
			$action = 'update';
		} else {
			$filtered_input['password_id'] = '';
			$action = 'insert';
		}

		if (isset($_POST['password_group_id']) && ctype_digit($_POST['password_group_id'])){
			$filtered_input['password_group_id'] = $_POST['password_group_id'];
		} else {
			$error .= ILLEGAL_GROUPNAME . '<br>';
		}

		$allowed_titel_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['titel'])) {
			if (preg_match($allowed_titel_characters, $_POST['titel'])){
				$filtered_input['titel'] = $_POST['titel'];
			} else {
				$error .= ILLEGAL_PASSWORDNAME . '<br>';
			}
		} else {
			$filtered_input['titel'] = '';
		}

		$allowed_name_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['name'])) {
			if (preg_match($allowed_name_characters, $_POST['name']) || trim($_POST['name']) == ''){
				$filtered_input['name'] = trim($_POST['name']);
			} else {
				$error .= ILLEGAL_USERNAME . '<br>';
			}
		} else {
			$filtered_input['name'] = '';
		}

		$allowed_notes_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['notes']) && $_POST['notes'] !='') {
			if (preg_match($allowed_notes_characters, $_POST['notes'])){
				$filtered_input['notes'] = $_POST['notes'];
			} else {
				$error .= ILLEGAL_NOTES_CHARACTERS . '<br>';
			}
		} else {
			$filtered_input['notes'] = '';
		}

		if (isset($_POST['url']) && trim($_POST['url']) != '') {
			$filter_options = array('flags' => FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED);
			if (filter_var(trim(strtr($_POST['url'],'-', '_')), FILTER_VALIDATE_URL, $filter_options)){
				$filtered_input['url'] = trim($_POST['url']);
			} else {
				$error .= ILLEGAL_URL . trim(strtr($_POST['url'],'-', '_'));
			}
		} else {
			$filtered_input['url'] = '';
		}

		$allowed_password_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['password']) && $_POST['password'] != '') {
			if (preg_match($allowed_password_characters, $_POST['password'])){
				$filtered_input['password'] = $_POST['password'];
				if (preg_match($allowed_password_characters, $_POST['password2'])){
						$filtered_input['password2'] = $_POST['password2'];
						if ($filtered_input['password'] != $filtered_input['password2']) {
							$error .= THE_PASSWORDS_DOESNT_MATCH . '<br>';
						}
				} else {
					$error .= WRONG_OR_MISSING_PASSWORD_REP . '<br>';
				}
			} else {
				$error .= ILLEGAL_PASSWORD . '<br>';
			}
		} else {
			if ($action == 'insert') {
				$error .= MISSING_PASSWORD . '<br>';
			}
		}



		$updated_password = new Passwords;
		$updated_password->PasswordGroupID = (isset($filtered_input['password_group_id']))?$filtered_input['password_group_id']:'';
		$updated_password->PasswordID      = (isset($filtered_input['password_id']))?$filtered_input['password_id']:'';
		$updated_password->Titel           = (isset($filtered_input['titel']))?$filtered_input['titel']:'';
		$updated_password->UserName        = (isset($filtered_input['name']))?$filtered_input['name']:'';
		$updated_password->Notes           = (isset($filtered_input['notes']))?$filtered_input['notes']:'';
		$updated_password->URL             = (isset($filtered_input['url']))?$filtered_input['url']:'';
		$updated_password->Password        = (isset($filtered_input['password']))?$filtered_input['password']:'';

		if ($error == '') {
			if ($action == 'update') {
				$updated_password->update($_COOKIE['phpPassSafe']);
			} elseif ($action == 'insert') {
				$logger->debug('inserting new password');
				$updated_password->insert($_COOKIE['phpPassSafe']);
			}
			$function_to_load = 'OnLoad="window.close()"';
			//$function_to_load = '';
		} else {
			//$function_to_load      = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['user_id'] . ')"';
			$function_to_load      = 'OnLoad="sndReq()"';
			$show_updated_password = true;
		}
	} else {
		if (ctype_digit($_POST['password_id'])){
			$filtered_input['password_id'] = $_POST['password_id'];
			$password_to_delete = new Passwords;
			$password_to_delete = $password_to_delete->fetch_details_for_password_id($filtered_input['password_id'], $_COOKIE['phpPassSafe']);
			$password_to_delete->delete($_COOKIE['phpPassSafe']);
			$filtered_input['password_id'] = false;
		}
		$function_to_load = 'OnLoad="window.close()"';
	}
}

$private_password_group = new PasswordGroups;
$private_password_group = $private_password_group->fetch_details_for_password_group_name($loggedin_user->UserID);



$password_groups = new PasswordGroups;
$passwords       = new Passwords;
$smarty          = new Smarty;

if (isset($filtered_input['password_id']) && ctype_digit($filtered_input['password_id'])) $smarty->assign('passworddetails', $passwords->fetch_details_for_password_id($filtered_input['password_id'], $_COOKIE['phpPassSafe']));
if ($show_updated_password) $smarty->assign('passworddetails', $updated_password);
$smarty->assign('password_groups', $password_groups->fetch_all_groups($loggedin_user->UserID));
if (isset($private_password_group->PasswordGroupID))$smarty->assign('private_password_group_id', $private_password_group->PasswordGroupID);
if (isset($filtered_input['preselct_group_id'])) $smarty->assign('preselct_group_id', $filtered_input['preselct_group_id']);
$smarty->assign('error', $error);
$smarty->assign('function_to_load', $function_to_load);
$smarty->assign('description', DESCRIPTION);
$smarty->assign('username', USERNAME);
$smarty->assign('password', PASSWORD);
$smarty->assign('passwordgroup', PASSWORDGROUP);
$smarty->assign('repeat_password', REPEAT_PASSWORD);
$smarty->assign('personal', PERSONAL);
$smarty->assign('url', URL);
$smarty->assign('note', NOTE);
$smarty->assign('ok', OK);
$smarty->assign('cancel', CANCEL);
$smarty->assign('delete', DELETE);
$smarty->assign('confirm_delete_password', CONFIRM_DELETE_PASSWORD);
$smarty->assign('generate_password', GENERATE_PASSWORD);

$smarty->display('edit_password_details.html');
?>
