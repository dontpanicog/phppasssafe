<?php
/*
 * Popup Fenster zum Editieren bestehender Passwort Gruppen
 * z.B. um diese User Gruppe zuzuordnen.
 */

require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/UserGroup_PasswordGroup_Mapping.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die('you are not logged in');
}



$error            = '';
$filtered_input   = array();
$function_to_load = '';

if (isset($_GET['Password_group_id'])) {
	if (ctype_digit($_GET['Password_group_id'])) {
		$filtered_input['Password_group_id'] = $_GET['Password_group_id'];
		$function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['Password_group_id'] . ')"';
	}
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'OK') {
		if (ctype_digit($_POST['Password_group_id'])){
			$filtered_input['Password_group_id'] = $_POST['Password_group_id'];
			$action = 'update';
		} else {

			$error .= 'fehlende oder ung&uuml;ltige Gruppen ID<br>';
		}


		if (ctype_alpha($_POST['name'])){
			$filtered_input['name'] = trim($_POST['name']);
		} else {
			$error .= 'fehlender oder ung&uuml;ltiger Gruppenname<br>';
		}


		if ($error == '') {
			$updated_password_group = new PasswordGroups;
			$updated_password_group->PasswordGroupID   = $filtered_input['Password_group_id'];
			$updated_password_group->PasswordGroupName = $filtered_input['name'];

			$updated_password_group->update($_COOKIE['phpPassSafe']);

			$function_to_load = 'OnLoad="window.close()"';
			//$function_to_load = '';
		} else {
			$function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['Password_group_id'] . ')"';
		}
	} else {
		if (ctype_digit($_POST['user_id'])){
			$filtered_input['user_id'] = $_POST['user_id'];
			$user_to_delete = new Users;
			$user_to_delete->UserID = $filtered_input['user_id'];
			$user_to_delete->delete();

		}
		$function_to_load = 'OnLoad="window.close()"';
	}
}






$password_groups  = new PasswordGroups;
$smarty = new Smarty;
if (isset($filtered_input['Password_group_id'])) $smarty->assign('password_group_details', $password_groups->fetch_details_for_password_group_id($filtered_input['Password_group_id']));
$smarty->assign('error', $error);
$smarty->assign('function_to_load', $function_to_load);
$smarty->display('password_group_details.html');
?>
