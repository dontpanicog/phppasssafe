<?php
/*
 * Popup Fenster zum Anlegen und editieren von Passwoertern
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/UserGroup_PasswordGroup_Mapping.php';
require_once 'model/Passwords.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
	die('you are not logged in');
}

$error            = '';
$filtered_input   = array();
$function_to_load = '';

if (isset($_GET['password_id'])) {
	if (ctype_digit($_GET['password_id'])) {
		$filtered_input['password_id'] = $_GET['password_id'];
	}
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'OK') {
		if (ctype_digit($_POST['password_id'])){
			$filtered_input['password_id'] = $_POST['password_id'];
			$action = 'update';
		} else {
			$filtered_input['password_id'] = '';
			$action = 'insert';
		}

		$allowed_titel_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['titel'])) {
			if (preg_match($allowed_titel_characters, $_POST['titel'])){
				$filtered_input['titel'] = $_POST['titel'];
			} else {
				$error .= 'ung&uuml;ltige Zeichen bei Passwort Bezeichnung<br>';
			}
		} else {
			$filtered_input['titel'] = '';
		}

		$allowed_name_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['name'])) {
			if (preg_match($allowed_name_characters, $_POST['name']) || trim($_POST['name']) == ''){
				$filtered_input['name'] = trim($_POST['name']);
			} else {
				$error .= 'ung&uuml;ltige Zeichen bei Anmelde Name<br>';
			}
		} else {
			$filtered_input['name'] = '';
		}

		$allowed_notes_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['notes']) && $_POST['notes'] !='') {
			if (preg_match($allowed_notes_characters, $_POST['notes'])){
				$filtered_input['notes'] = $_POST['notes'];
			} else {
				$error .= 'ung&uuml;ltige Zeichen bei Notiz<br>';
			}
		} else {
			$filtered_input['notes'] = '';
		}

		if (isset($_POST['url']) && trim($_POST['url']) != '') {
			$filter_options = array('flags' => FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED);
			if (filter_var(trim($_POST['url']), FILTER_VALIDATE_URL, $filter_options)){
				$filtered_input['url'] = filter_var(trim($_POST['url']), FILTER_VALIDATE_URL, $filter_options);
			} else {
				$error .= 'ung&uuml;ltige URL';
			}
		} else {
			$filtered_input['url'] = '';
		}

		$allowed_password_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['password']) && $_POST['password'] != '') {
			if (preg_match($allowed_password_characters, $_POST['password'])){
				$filtered_input['password'] = $_POST['password'];
				if (preg_match($allowed_password_characters, $_POST['password2'])){
						$filtered_input['password2'] = $_POST['password2'];
						if ($filtered_input['password'] != $filtered_input['password2']) {
							$error .= 'die beiden Passworteingaben stimmen nicht &uuml;berein<br>';
						}
				} else {
					$error .= 'fehlende oder falsche Passwortwiederholung<br>';
				}
			} else {
				$error .= 'Passwort enth&auml;lt ung&uuml;ltige Zeichen<br>';
			}
		} else {
			if ($action == 'insert') {
				$error .= 'es wurde kein Passwort angegeben<br>';
			}
		}




			$updated_password = new Passwords;
			//$updated_password->PasswordGroupID = $loggedin_user->UserID;
			$updated_password->PasswordID      = $filtered_input['password_id'];
			$updated_password->Titel           = $filtered_input['titel'];
			$updated_password->UserName        = $filtered_input['name'];
			$updated_password->Notes           = $filtered_input['notes'];
			$updated_password->URL             = $filtered_input['url'];
			$updated_password->Password        = (isset($filtered_input['password']))?$filtered_input['password']:'';
		if ($error == '') {
			if ($action == 'update') {
				$updated_password->update($_COOKIE['phpPassSafe']);
			} elseif ($action == 'insert') {
				$updated_password->insert($_COOKIE['phpPassSafe']);
			}
			$function_to_load = 'OnLoad="window.close()"';
			//$function_to_load = '';
		} else {
			$function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['user_id'] . ')"';
		}
	} else {
		if (ctype_digit($_POST['password_id'])){
			$filtered_input['password_id'] = $_POST['password_id'];
			$password_to_delete = new Passwords;
			$password_to_delete->PasswordID = $filtered_input['password_id'];
			$password_to_delete->delete($_COOKIE['phpPassSafe']);
			$filtered_input['password_id'] = false;
		}
		$function_to_load = 'OnLoad="window.close()"';
	}
}






$passwords       = new Passwords;
$smarty          = new Smarty;

if ($filtered_input['password_id']) $smarty->assign('passworddetails', $passwords->fetch_details_for_password_id($filtered_input['password_id'], $_COOKIE['phpPassSafe']));
$smarty->assign('error', $error);
$smarty->assign('function_to_load', $function_to_load);
$smarty->display('edit_private password_details.html');
?>
