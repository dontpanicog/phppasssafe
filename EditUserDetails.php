<?php
/*
 * Popup Fenster zum Editieren und Anlegen von Usern
 * z.B. um diese User Gruppe zuzuordnen.
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/Passwords.php';
require_once 'model/PasswordGroups.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;

$validCharacters = array('-', '_', '.', '/', '\\');

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
    if (isset($_GET['user_id']) && $_GET['user_id'] != $loggedin_user->UserID && $loggedin_user->UserID != ADMIN_UID && !isset($_POST['action'])) die('You are not allowed to change other users details');
} else {
	die('you are not logged in');
}

$user = new Users;
$loggedin_user = $user->check_credentials_from_cookie($cookie_value = $_COOKIE['phpPassSafe']);



$error            = '';
$filtered_input   = array();
$function_to_load = '';

if (isset($_GET['user_id'])) {
	if (ctype_digit($_GET['user_id'])) {
		$filtered_input['user_id'] = $_GET['user_id'];
		$function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['user_id'] . ')"';
	}
}

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'OK') {
		if (ctype_digit($_POST['user_id'])){
			$filtered_input['user_id'] = $_POST['user_id'];
			$action = 'update';
		} else {
			$filtered_input['user_id'] = '';
			$action = 'insert';
		}

		$allowed_name_characters = ALLOWED_CHARACTERS;

		if (isset($_POST['first_name'])) {
			if (preg_match($allowed_name_characters, $_POST['first_name']) || trim($_POST['first_name']) == ''){
				$filtered_input['first_name'] = trim($_POST['first_name']);
			} else {
				$error .= 'ung&uuml;ltige Zeichen im Vornamen<br>';
			}
		} else {
			$filtered_input['first_name'] = '';
		}

			if (isset($_POST['last_name'])) {
			if (preg_match($allowed_name_characters, $_POST['last_name']) || trim($_POST['last_name']) == ''){
				$filtered_input['last_name'] = trim($_POST['last_name']);
			} else {
				$error .= 'ung&uuml;ltige Zeichen im Nachnamen<br>';
			}
		} else {
			$filtered_input['last_name'] = '';
		}

		if (ctype_alnum(str_replace($validCharacters, '', $_POST['name']))){
			$filtered_input['name'] = trim($_POST['name']);
		} else {
			$error .= 'fehlender oder ung&uuml;ltiger Anmeldename: ' . str_replace($validCharacters, '', $_POST['name']) . '<br>';
		}

		$allowed_email_characters = ALLOWED_EMAIL_CHARACTERS;
		if ($_POST['email']) {
			if (preg_match($allowed_email_characters, $_POST['email']) || trim($_POST['email']) == ''){
				$filtered_input['email'] = trim($_POST['email']);
			} else {
				$error .= 'ung&uuml;ltige Email Adresse<br>';
			}
		} else {
			$filtered_input['email'] = '';
		}

		$allowed_password_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['password']) && $_POST['password'] != '') {
			if (preg_match($allowed_password_characters, $_POST['password'])){
				$filtered_input['password'] = trim($_POST['password']);
				if (preg_match($allowed_password_characters, $_POST['password2'])){
						$filtered_input['password2'] = $_POST['password2'];
						if ($filtered_input['password'] != $filtered_input['password2']) {
							$error .= 'die beiden Passworteingaben stimmen nicht &uuml;berein<br>';
						}
				} else {
					$error .= 'fehlende oder falsche Passwortwiederholung<br>';
				}
			} else {
				$error .= 'Passwort enth&auml;lt ung&uuml;ltige Zeichen<br>';
			}
		} else {
			if ($action == 'insert') {
				$error .= 'es wurde kein Passwort angegeben<br>';
			}
		}



		if (isset ($_POST['active'])) {
			if($_POST['active'] == 'on') $filtered_input['active'] = true;
		}

		if ($error == '') {
			$updated_user = new Users;
			$updated_user->UserID       = $filtered_input['user_id'];
			$updated_user->FirstName    = $filtered_input['first_name'];
			$updated_user->LastName     = $filtered_input['last_name'];
			$updated_user->Name         = $filtered_input['name'];
			$updated_user->email        = $filtered_input['email'];
			$updated_user->UserPassword = (isset($filtered_input['password']))?$filtered_input['password']:'';
			$updated_user->active       = $filtered_input['active'];
			if ($action == 'update') {
				$updated_user->update($_COOKIE['phpPassSafe']);
			} elseif ($action == 'insert') {
				$updated_user->insert($_COOKIE['phpPassSafe']);
			}
			$function_to_load = 'OnLoad="window.close()"';
			//$function_to_load = '';
		} else {
			$function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['user_id'] . ')"';
		}
	} else {
		if (ctype_digit($_POST['user_id'])){
			$filtered_input['user_id'] = $_POST['user_id'];
			$user_to_delete = new Users;
			$user_to_delete->UserID = $filtered_input['user_id'];
			$user_to_delete->delete();

		}
		$function_to_load = 'OnLoad="window.close()"';
	}
}






$users  = new Users;
$smarty = new Smarty;
if (isset($filtered_input['user_id'])) {
	$smarty->assign('userdetails', $users->fetch_details_for_user_id($filtered_input['user_id']));
} else {
    $smarty->assign('userdetails', $users);
}
$smarty->assign('loggedin_user', $loggedin_user);
$smarty->assign('admin_id', ADMIN_UID);
$smarty->assign('error', $error);
$smarty->assign('function_to_load', $function_to_load);

$smarty->assign('username', USERNAME);
$smarty->assign('firstname', FIRSTNAME);
$smarty->assign('lastname', LASTNAME);
$smarty->assign('email', EMAIL);
$smarty->assign('password', PASSWORD);
$smarty->assign('repeat_password', REPEAT_PASSWORD);
$smarty->assign('active', ACTIVE);
$smarty->assign('ok', OK);
$smarty->assign('cancel', CANCEL);
$smarty->assign('delete', DELETE);
$smarty->assign('confirm_delete_user', CONFIRM_DELETE_USER);
$smarty->display('user_details.html');
?>
