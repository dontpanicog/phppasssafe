<?php
require_once 'Smarty/Smarty.class.php';
require_once 'Config.php';
require_once 'lang/' . LANGUAGE;


$smarty = new Smarty;
$smarty->assign('generated_password', GENERATED_PASSWORD);
$smarty->assign('generate_password', GENERATE_PASSWORD);
$smarty->assign('upper_case', UPPER_CASE);
$smarty->assign('lower_case', LOWER_CASE);
$smarty->assign('digits', DIGITS);
$smarty->assign('special_characters', SPECIAL_CHARACTERS);
$smarty->assign('length', LENGTH);
$smarty->assign('generate_random_password', GENERATE_RANDOM_PASSWORD);
$smarty->assign('cancel', CANCEL);
$smarty->assign('accept', ACCEPT);
$smarty->assign('prefix', GENERATED_PASSWORD_PREFIX);
$smarty->assign('preselect_lowercase', PRESELECT_LOWERCASE);
$smarty->assign('preselect_uppercase', PRESELECT_UPPERCASE);
$smarty->assign('preselect_specialcharacters', PRESELECT_SPECIALCHARACTERS);
$smarty->assign('preselect_digits', PRESELECT_DIGITS);
$smarty->assign('default_lenght', DEFAULT_LENGHT);
$smarty->display('generate_password.html');