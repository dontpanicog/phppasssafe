<?php
/*
 * Passwortexport
*/
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/Passwords.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);

	if ($loggedin_user) {
		
		$filename    = 'passwords.csv';
		$application = 'text/csv';

		
		$password_groups = new PasswordGroups;
		$passwords       = new Passwords;

// 		$private_password_group_id = PasswordGroups::fetch_group_id_for_group_name($loggedin_user->UserID);
// 		$password_group_list = array();
// 		$password_details    = array();
// 		$password_group_list[$private_password_group_id] = 'Pers&ouml;nliche Passw&ouml;rter';
// 		$filter['password_group_id']      = $private_password_group_id;
// 		foreach ($passwords->fetch_all_passwords($filter, $loggedin_user->UserID) as $password) {
// 			$password_list[$private_password_group_id][$password->PasswordID] = htmlentities($password->Titel, 0, "ISO-8859-1");
// 			$password_details[$password->PasswordID] = new Passwords;
// 			$password_details[$password->PasswordID] = $password_details[$password->PasswordID]->fetch_details_for_password_id($password->PasswordID, $_COOKIE['phpPassSafe']);
// 		}
		$csv = '';
		if (is_array($password_groups->fetch_all_groups($loggedin_user->UserID))) {
			foreach ($password_groups->fetch_all_groups($loggedin_user->UserID) as $group) {
				//$password_group_list[$group->PasswordGroupID] = htmlentities($group->PasswordGroupName, 0, "ISO-8859-1");
				// 'Bezeichnung', 'Anmeldename', 'Passwort', 'Passwortgruppe', 'URL', 'Notiz'
				
				$filter['password_group_id']      = $group->PasswordGroupID;
				
				foreach ($passwords->fetch_all_passwords($filter, $loggedin_user->UserID) as $password) {

					$csv .= $password->Titel . ',';
					$password_details = new Passwords;
					$password_details = $password_details->fetch_details_for_password_id($password->PasswordID, $_COOKIE['phpPassSafe']);
					$csv .= $password_details->UserName . ',';
					$csv .= $password_details->Password . ',';
                    $csv .= $group->PasswordGroupName . ', ';
					$csv .= $password_details->URL . ',';
					$csv .= $password_details->Notes . "\n";
				}
			}
		}
		header( "Content-Type: $application" );
		header( "Content-Disposition: attachment; filename=$filename");
		header( "Content-Description: csv File" );
		header( "Pragma: no-cache" );
		header( "Expires: 0" );
		echo $csv;


	} else {
		die('Access denied');
	}

} else {
	header("location: index.php");
}


?>
