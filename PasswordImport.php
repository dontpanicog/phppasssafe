<?php
/*
 * Bulkimport von Passw�rtern im CSV Format
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/Passwords.php';
require_once 'model/PasswordGroups.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'controller/Logger.php';

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);

	$template        = 'password_import.html';
	$imported_items  = array();


    if ($loggedin_user->UserID == ADMIN_UID) {
    	$error = '';
    	if (isset($_POST['import'])) {
			if ($_POST['import'] == 'upload') {
				if (isset($_FILES['PasswordFile']['tmp_name'])             &&
				    $_FILES['PasswordFile']['tmp_name'] != 'none'          &&
				    $_FILES['PasswordFile']['error'] == 'UPLOAD_ERR_OK'    &&
				    $_FILES['PasswordFile']['size'] > 0                    &&
				    is_uploaded_file($_FILES['PasswordFile']['tmp_name'])) {

				    $file = fopen($_FILES['PasswordFile']['tmp_name'], 'r');

				    while (($line = fgetcsv ($file, 2000, ",", "'")) !== FALSE ) {

				    	// Bezeichnung
				    	$allowed_titel_characters = ALLOWED_CHARACTERS;
						if (isset($line[0])) {
							if (preg_match($allowed_titel_characters, $line[0])){
								$filtered_input['titel'] = $line[0];
							} else {
								$error .= 'ung&uuml;ltige Zeichen bei Passwort Bezeichnung: '. htmlentities($line[0], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['titel'] = '';
						}

				    	// Username
				    	$allowed_name_characters = ALLOWED_CHARACTERS;
						if (isset($line[1])) {
							if (preg_match($allowed_titel_characters, $line[1])){
								$filtered_input['name'] = $line[1];
							} else {
								$error .= 'ung&uuml;ltige Zeichen bei Username: '. htmlentities($line[1], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['name'] = '';
						}

				    	// Passwort
				    	$allowed_password_characters = ALLOWED_CHARACTERS;
						if (isset($line[2])) {
							if (preg_match($allowed_password_characters, $line[2])){
								$filtered_input['password'] = $line[2];
							} else {
								$error .= 'ung&uuml;ltige Zeichen bei Passwort mit der Bezeichnung: '. htmlentities($line[0], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['password'] = '';
						}

				    	// Gruppe
						if (isset($line[0])) {
							if (preg_match($allowed_name_characters, $line[3])){
								$filtered_input['group'] = $line[3];
								$password_group = new PasswordGroups;
								$password_group = $password_group->fetch_details_for_password_group_name($filtered_input['group']);
								if (!$password_group) { // Die Gruppe gibt es noch nicht
									$error .= 'Die Gruppe: ' . htmlentities($line[3], 0, "ISO-8859-1") . ' existiert nicht.<br>';
								} else {
									$filtered_input['group_id'] = $password_group->PasswordGroupID;
								}
							} else {
								$error .= 'ung&uuml;ltige Zeichen bei Gruppe: ' . htmlentities($line[3], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['group'] = '';
						}

				    	// URL
					    if (isset($line[4]) && trim($line[4]) != '') {
							$filter_options = array('flags' => FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED);
							if (filter_var(trim($line[4]), FILTER_VALIDATE_URL, $filter_options)){
								$filtered_input['url'] = filter_var(trim($line[4]), FILTER_VALIDATE_URL, $filter_options);
							} else {
								$error .= 'ung&uuml;ltige URL: ' . htmlentities($line[4], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['url'] = '';
						}

				    	// Notiz
				    	$allowed_notes_characters = ALLOWED_CHARACTERS;
						if (isset($line[5])) {
							if (preg_match($allowed_notes_characters, $line[5]) || $line[5] == ''){
								$filtered_input['notes'] = $line[5];
							} else {
								$error .= 'ung&uuml;ltige Zeichen bei Passwort Notiz: '. htmlentities($line[5], 0, "ISO-8859-1") . '<br>';
							}
						} else {
							$filtered_input['notes'] = '';
						}

						if ($error == '') {
							$password = new Passwords;
							$password->Titel           = $filtered_input['titel'];
							$password->UserName        = $filtered_input['name'];
							$password->Password        = $filtered_input['password'];
							$password->PasswordGroupID = $filtered_input['group_id'];
							$password->URL             = $filtered_input['url'];
							$password->Notes           = $filtered_input['notes'];

							$password->insert($_COOKIE['phpPassSafe']);
						}

						$imported_items[] = $filtered_input;

				    }
				    $template = 'validate_upload.html';


				} else {
					$error .= 'es gab ein Problem mit dem Fileupload';
				}
			}
    	}

		if ($error == '') $error = 'Die oben dargestellten Datensätze wurden erfolgreich importiert';


		$smarty = new Smarty;
		$smarty->assign('admin_id', ADMIN_UID);
		$smarty->assign('user_name', $loggedin_user->Name);
		$smarty->assign('user_id', $loggedin_user->UserID);
		$smarty->assign('error', $error);
		$smarty->assign('imported_items', $imported_items);
		$smarty->display($template);
    } else {
    	die('Access denied');
    }

} else {
    	header("location: index.php");
}


?>