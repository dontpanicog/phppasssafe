<?php
/*
 * Zentrales Script zum Passwortmanagement
 * die Eigentlichen Inhalte werden durch
 * Javascripte in fetch_password_details.js und fetch_password_entries.js
 * nachgeladen
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/Passwords.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;
require_once 'controller/Logger.php';
require_once 'controller/idoit.php';

header("Expires: ".gmdate("D, d M Y H:i:s")." GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Pragma: no-cache");
header("Cache-Control: post-check=0, pre-check=0", false);


$logger = new Logger;
$filter = array(
	'page' => '',
	'titel' => '',
	'password_group_id' => ''
);


if (isset($_GET['p']) && ctype_digit($_GET['p'])) {
    $page = $_GET['p'];
} else {
    $page = 1;
}

$allowed_search_characters = ALLOWED_CHARACTERS;
if (isset($_GET['search']) && preg_match($allowed_search_characters, $_GET['search'])) {
	$filter['titel'] = $_GET['search'];
} else {
	$filter['titel'] = '';
}

if (isset($_GET['password_group_id']) && ctype_digit($_GET['password_group_id'])) {
	$filter['password_group_id'] = $_GET['password_group_id'];
} else {
	$filter['password_group_id'] = '';
}



if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
	if (!$loggedin_user) header("location: index.php");

    $password_groups = new PasswordGroups;
    $user_groups = new Groups();

    $private_password_group_id = PasswordGroups::fetch_group_id_for_group_name($loggedin_user->UserID);

    $filter['page'] = $page;
    $passwords = new Passwords;

    $count = $passwords->count($filter, $loggedin_user->UserID);
    $pages = ((int) (($count-1)/20));

	if (defined('IDOITURL') && defined('IDOITKEY')) {
		$search = '';
		$idoit = new idoit();
		$idoit->apikey = IDOITKEY;
		$idoit->apiurl = IDOITURL;
		$idoit_objs = $idoit->receive_objects($search);
	}


    if ($loggedin_user) {
		$smarty = new Smarty;
		if (isset($idoit_objs)) $smarty->assign('idoit_objs', $idoit_objs);
		$smarty->assign('inactivity_timeout', INACTIVITYTIMEOUT);
		$smarty->assign('displaytime', DISPLAYTIME);
		$smarty->assign('admin_id', ADMIN_UID);
		$smarty->assign('user_name', $loggedin_user->Name);
		$smarty->assign('user_id', $loggedin_user->UserID);
		$smarty->assign('loggedin_user', $loggedin_user);
		$smarty->assign('add_passwordgroup', ADD_PASSWORDGROUP);
		$smarty->assign('add_password', ADD_PASSWORD);
		$smarty->assign('export', EXPORT);
		$smarty->assign('import', IMPORT);
		$smarty->assign('refresh', REFRESH);
		$smarty->assign('usermanagement', USERMANAGEMENT);
		$smarty->assign('change_password', CHANGE_PASSWORD);
		$smarty->assign('logout', LOGOUT);
		$smarty->assign('groups', GROUPS);
		$smarty->assign('username', USERNAME);
		$smarty->assign('description', DESCRIPTION);
		$smarty->assign('note', NOTE);
		$smarty->assign('group', GROUP);
		$smarty->assign('clear', CLEAR);
		$smarty->assign('clear_filter', CLEAR_FILTER);
		$smarty->assign('clear_message', CLEAR_MESSAGE);
		$smarty->assign('search_password', SEARCH_PASSWORD);
		$smarty->assign('generated_password_prefix', GENERATED_PASSWORD_PREFIX);
		$smarty->assign('preselect_digits', PRESELECT_DIGITS);
		$smarty->assign('preselect_lowercase', PRESELECT_LOWERCASE);
		$smarty->assign('preselect_specialcharacters', PRESELECT_SPECIALCHARACTERS);
		$smarty->assign('preselect_uppercase', PRESELECT_UPPERCASE);
        $smarty->assign('default_length', DEFAULT_LENGHT);
		$smarty->assign('lower_case', LOWER_CASE);
		$smarty->assign('upper_case', UPPER_CASE);
		$smarty->assign('digits', DIGITS);
		$smarty->assign('special_characters', SPECIAL_CHARACTERS);
		$smarty->assign('length', LENGTH);
		$smarty->assign('filter', $filter);
        $smarty->assign('all_groups', ALL);
        $smarty->assign('confirm_delete', CONFIRM_DELETE_PASSWORD);
        $smarty->assign('close', CLOSE);
        $smarty->assign('delete', DELETE);
        $smarty->assign('save', SAVE);
        $smarty->assign('cancel', CANCEL);
        $smarty->assign('all', ALL);
		$smarty->assign('personel', PERSONAL);
		$smarty->assign('first_name', FIRSTNAME);
		$smarty->assign('last_name', LASTNAME);
		$smarty->assign('user_name', USERNAME);
		$smarty->assign('email', EMAIL);
		$smarty->assign('password', PASSWORD);
		$smarty->assign('repeat_password', REPEAT_PASSWORD);
		$smarty->assign('user_settings', USERDETAILS);
		$smarty->assign('_password_group', PASSWORDGROUP);
		$smarty->assign('display_password', DISPLAY_PASSWORD);
		$smarty->assign('copy_password_to_clipboard', COPY_PASSWORD_TO_CLIPBOARD);
		$smarty->assign('display_password_generator', DISPLAY_PASSWORD_GENERATOR);

		$smarty->assign('password_groups', $password_groups->fetch_all_groups($loggedin_user->UserID, true));
        $smarty->assign('user_groups', $user_groups->fetch_all_groups());
        $smarty->assign('passwords', $passwords->fetch_all_passwords($filter, $loggedin_user->UserID));
        $smarty->assign('page', $page);
        if ($page <=1) {
            $smarty->assign('previous_page', $page);
        } else {
            $smarty->assign('previous_page', $page-1);
        }
        if ($page >= $pages) {
            $smarty->assign('next_page', $page);
        } else {
            $smarty->assign('next_page', $page+1);
        }

        $smarty->assign('pages', $pages);

		$smarty->display('password_management.html');
    } else {
    	die('Access denied');
    }

} else {
	$logger->debug('No Cookie found');
    header("location: index.php");
}


?>
