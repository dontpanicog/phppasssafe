<?php
/*
 * Zentrales Script zum Usermanagement
 * die Eigentlichen Inhalte werden durch
 * Javascripte in fetchdetails.js und fetchentries.js
 * nachgeladen
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;


header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

if (isset($_COOKIE['phpPassSafe'])) {
	$user            = new Users;
	$loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);

    if ($loggedin_user && $loggedin_user->UserID == ADMIN_UID) {
    	$smarty = new Smarty;
		$smarty->assign('user', $user);
		$smarty->assign('user_name', $loggedin_user->Name);
		$smarty->assign('user_id', $loggedin_user->UserID);
		$smarty->assign('add_usergroup', ADD_USERGROUP);
		$smarty->assign('add_user', ADD_USER);
		$smarty->assign('refresh', REFRESH);
		$smarty->assign('change_to_passwordmanagement', CHANGE_TO_PASSWORDMANAGEMENT);
		$smarty->assign('change_password', CHANGE_PASSWORD);
		$smarty->assign('logout', LOGOUT);
		$smarty->assign('groups', GROUPS);
		$smarty->assign('username', USERNAME);
		$smarty->assign('firstname', FIRSTNAME);
		$smarty->assign('lastname', LASTNAME);
		$smarty->assign('email', EMAIL);
		$smarty->display('user_management.html');
    } else {
		setcookie("phpPassSafe", '', -9999, '', '', 0);
    	die('Access denied');
    }

} else {
    	header("location: index.php");
}
?>
