<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../lang/' . LANGUAGE;
require_once '../Smarty/Smarty.class.php';

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die ('');
}

if (isset($_GET['group_id']) && ctype_digit($_GET['group_id']) && isset($_GET['action']) && $_GET['action'] == 'delete') {
	$groups = new Groups;
	$groups->GroupID = $_GET['group_id'];
	$groups = $groups->fetch_details_for_group($groups->GroupID);
	$groups->delete();
}

$groups = new Groups;

echo '<table style="width: 100%;">';
if (is_array($groups->fetch_all_groups())) {
	foreach ($groups->fetch_all_groups() as $group) {
		if ($group->UserContainer) continue;
		echo '<tr class="itemlist">';
		echo '<td class="itemlist" onClick="confirmDelete(' . $group->GroupID . ', \'' . htmlentities(CONFIRM_DELETE_USERGROUP, 0) . '\');"><img src="./images/delete-12.png" title="' . htmlentities(DELETE_GROUP, 0) . '"></td>';
		echo '<td class="itemlist" onClick="sndUserReq(' . $group->GroupID . ')">' . htmlentities($group->GroupName, 0) . '</td>';
		echo '</tr>';
	}
}
?>
