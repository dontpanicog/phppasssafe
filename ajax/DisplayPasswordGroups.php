<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../Smarty/Smarty.class.php';
require_once '../lang/' . LANGUAGE;

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user) die('you are not logged in');
} else {
	die ('');
}

if (isset($_GET['group_id']) && ctype_digit($_GET['group_id']) && isset($_GET['action']) && $_GET['action'] == 'delete') {
	$groups = new PasswordGroups;
	$groups->PasswordGroupID = $_GET['group_id'];
	$groups->delete();
}

$password_groups = new PasswordGroups;

$private_password_group_id = PasswordGroups::fetch_group_id_for_group_name($loggedin_user->UserID);

echo '<table style="width: 100%;">';
if ($loggedin_user->UserID != ADMIN_UID) {
	echo '<tr class="itemlist">';
	echo '<td class="itemlist" onClick="sndPasswordReq(' . $private_password_group_id . ')">' . htmlentities(PERSONEL_PASSWORDS, 0, "UTF-8") . '</td>';
	echo '</tr>';
}
if (is_array($password_groups->fetch_all_groups($loggedin_user->UserID))) {


	foreach ($password_groups->fetch_all_groups($loggedin_user->UserID) as $group) {
		echo '<tr class="itemlist">';
		if ($loggedin_user->UserID == ADMIN_UID) {
			echo '<td width="20px" class="itemlist" onClick="ResetCounter(); confirmDelete(' . $group->PasswordGroupID . ', \'' . htmlentities(CONFIRM_DELETE_PASSWORDGROUP, 0) . '\');"><img src="./images/delete-12.png" alt="' . htmlentities(DELETE_PASSWORDGROUP, 0) . '" title="' . htmlentities(DELETE_PASSWORDGROUP, 0) . '"></td>';
		}
		echo '<td class="itemlist" onClick="ResetCounter(); sndPasswordReq(' . $group->PasswordGroupID . ');">' . htmlentities($group->PasswordGroupName, 0) . '</td>';
		echo '</tr>';
	}

	echo '<tr class="itemlist">';
	echo '<td class="itemlist" onClick="ResetCounter(); sndPasswordReq(\'all\');">' . htmlentities(ALL, 0) . '</td>';
	echo '</tr>';
}
?>