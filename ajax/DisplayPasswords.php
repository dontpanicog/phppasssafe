<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../Smarty/Smarty.class.php';
require_once '../lang/' . LANGUAGE;


$date = array();
$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user) die('you are not logged in');
} else {
	die ('');
}

$filter = array(
	'page' => '',
	'titel' => '',
	'password_group_id' => ''
);


if (isset($_GET['p']) && ctype_digit($_GET['p'])) {
	$filter['page'] = $_GET['p'];
} else {
	$filter['page'] = 1;
}

$allowed_search_characters = ALLOWED_CHARACTERS;
if (isset($_GET['titel']) && preg_match($allowed_search_characters, $_GET['titel'])) {
	$filter['titel'] = $_GET['titel'];
} else {
	$filter['titel'] = '';
}

if (isset($_GET['password_group_id']) && ctype_digit($_GET['password_group_id'])) {
	$filter['password_group_id'] = $_GET['password_group_id'];
} else {
	$filter['password_group_id'] = '';
}

$passwords = new Passwords();
$data['passwords'] = $passwords->fetch_all_passwords($filter, $loggedin_user->UserID);
$data['count']     = $passwords->count($filter,$loggedin_user->UserID);

echo json_encode($data);

$logger = new Logger();
$logger->debug(print_r($data, true));

?>
