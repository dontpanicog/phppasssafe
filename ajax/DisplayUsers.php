<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../lang/' . LANGUAGE;
require_once '../Smarty/Smarty.class.php';

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die ('no cookie value');
}

$filter = array();
$filter['name']      = '';
$filter['firstname'] = '';
$filter['lastname']  = '';
$filter['email']     = '';
$filter['order']     = '';
$filter['group_id']  = '';
$filter['order_by']  = 'Name';

if (ctype_digit($_GET['group_id'])) $filter['group_id'] = $_GET['group_id'];
if (ctype_alnum($_GET['name']))     $filter['name']     = $_GET['name'];

$users = new Users;
echo '<table style="width: 100%;">';

foreach ($users->fetch_all_users($filter) as $user) {

	if (strlen($user->Name)>19) {
		$user_name = substr($user->Name, 0, 17) . '...';
	} else {
		$user_name = $user->Name;
	}

	if (strlen($user->FirstName)>19) {
		$first_name = substr($user->FirstName, 0, 17) . '...';
	} else {
		$first_name = $user->FirstName;
	}

	if (strlen($user->LastName)>19) {
		$last_name = substr($user->LastName, 0, 17) . '...';
	} else {
		$last_name = $user->LastName;
	}

	if (strlen($user->email)>10) {
		$email = substr($user->email, 0, 8) . '...';
	} else {
		$email = $user->email;
	}


	if ($user->UserID == ADMIN_UID) continue;
	$user->active == 0 ? $color = 'style="color:#8c8c8d;"' : $color = '';
	echo '<tr ' . $color . ' class="itemlist" onClick="sndReq(\'userdetails\',' . $user->UserID . ')">';
	echo '<td width="20px" class="itemlist"><a href="EditUserDetails.php?user_id=' . $user->UserID . '" onclick="openwindow(\'EditUserDetails.php?user_id=' . $user->UserID . '\'); return false"><img alt="' . htmlentities(EDIT_USERDETAILS, 0) . '" src="images/edit-17.png" title="' . htmlentities(EDIT_USERDETAILS, 0, "UTF-8") . '"></img></a></td>';
	echo '<td width="140px" class="itemlist">' . htmlentities($user_name, 0, "UTF-8") . '</td>';
	echo '<td width="120px" class="itemlist">' . htmlentities($first_name, 0, "UTF-8") . '</td>';
	echo '<td width="120px" class="itemlist">' . htmlentities($last_name, 0, "UTF-8") . '</td>';
	echo '<td class="itemlist">' . htmlentities($email, 0, "UTF-8") . '</td>';
	echo '</tr>';
}

echo '</table>';
?>



