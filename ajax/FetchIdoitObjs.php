<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 26.09.16
 * Time: 16:20
 */

require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../model/Users_Groups_Mapping.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../lang/' . LANGUAGE;
require_once '../controller/idoit.php';


$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
    $user = new Users;
    $loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
    die ('no cookie value');
}

if (isset($_GET['search'])){
    $filtered_input['search'] = $_GET['search'];
} else {
    $filtered_input['search'] = '';
}



$idoit = new idoit();

$idoit->apikey = IDOITKEY;
$idoit->apiurl = IDOITURL;


$search = '';
echo json_encode($idoit->receive_objects($search));


