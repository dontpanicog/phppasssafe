<?php

require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../Smarty/Smarty.class.php';

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die ('no cookie value');
}

$groups = new Groups;
$users  = new Users;

if (isset($_GET['user_id'])) {
	if (ctype_digit($_GET['user_id'])) {
		$user_id          = $_GET['user_id'];
		if (isset($_GET['group_id'])) {
			if (ctype_digit($_GET['group_id'])) {
				$group_id          = $_GET['group_id'];
			}
		}
		if (isset($_GET['direction'])) {
			if ($_GET['direction'] == 'from_available_to_selected') {
				$users->add_user_to_group($user_id, $group_id, $_COOKIE['phpPassSafe']);
			} elseif ($_GET['direction'] == 'from_selected_to_available') {
				$users->del_user_from_group($user_id, $group_id);
			}
		}

		$user_details     = $users->fetch_details_for_user_id($_GET['user_id']);
	}
}




?>

<div id="AvailableGroups">
<?php
foreach ($user_details->AvailableGroups as $available_group) {
	if ($available_group['UserContainer'] == true) continue;
	echo '<span OnClick="sndReq(\'from_available_to_selected\', ' . $available_group['GroupID'] . ', ' . $user_id . ')">' . htmlentities($available_group['GroupName'], 0) . '</span><br />';
	//var_dump($available_group);
}
?>
</div>

<div id="SelectedGroups">
<?php
foreach ($user_details->JoinedGroups as $selected_group) {
	if ($selected_group['UserContainer'] == true) continue;
	echo '<span OnClick="sndReq(\'from_selected_to_available\', ' . $selected_group['GroupID'] . ', ' . $user_id . ')">' . htmlentities($selected_group['GroupName'], 0) . '</span><br />';
	//var_dump($selected_group);
}
?>
</div>



