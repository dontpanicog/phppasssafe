<?php

require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../controller/Crypto.php';
require_once '../Smarty/Smarty.class.php';
require_once '../controller/Logger.php';

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die ('no cookie value');
}

$groups          = new Groups;
$password_groups = new PasswordGroups;

if (isset($_GET['password_group_id'])) {
	if (ctype_digit($_GET['password_group_id'])) {
		$password_group_id = $_GET['password_group_id'];
		if (isset($_GET['user_group_id'])) {
			if (ctype_digit($_GET['user_group_id'])) {
				$user_group_id = $_GET['user_group_id'];
			}
		}
		if (isset($_GET['direction'])) {
			if ($_GET['direction'] == 'from_available_to_selected') {
				$password_groups->add_password_group_to_user_group($password_group_id, $user_group_id, $_COOKIE['phpPassSafe']);
			} elseif ($_GET['direction'] == 'from_selected_to_available') {
				$password_groups->del_password_group_from_user_group($password_group_id, $user_group_id);
			}
		}

		$password_group_details     = $password_groups->fetch_details_for_password_group_id($_GET['password_group_id']);
	}
}




?>

<div id="AvailableGroups">
<?php

foreach ($password_group_details->AvailableGroups as $group_id => $group_name) {
	echo '<span OnClick="sndReq(\'from_available_to_selected\', ' . $group_id . ', ' . $password_group_id . ')">' . htmlentities($group_name, 0) . '</span><br />';

}
?>
</div>

<div id="SelectedGroups">
<?php
foreach ($password_group_details->MappedUserGroups as $group_id => $group_name) {
	echo '<span OnClick="sndReq(\'from_selected_to_available\', ' . $group_id . ', ' . $password_group_id . ')">' . htmlentities($group_name, 0) . '</span><br />';
	//var_dump($selected_group);
}
?>
</div>
