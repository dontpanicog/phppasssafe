<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 28.06.16
 * Time: 07:52
 */
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';


$logger = new Logger;
$logger->debug('EditPasswordDetails was called');
$show_updated_password = false;

if (isset($_COOKIE['phpPassSafe'])) {
    $user            = new Users;
    $loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
    die('you are not logged in');
}

$error            = array();
$filtered_input   = array();




if (ctype_digit($_GET['PasswordID'])){
    $filtered_input['PasswordID'] = $_GET['PasswordID'];
    $action = 'delete';
} else {
    $filtered_input['PasswordID'] = '';
    $action = '';
}

$password = new Passwords();
$password->PasswordID = $filtered_input['PasswordID'];
$password->delete($_COOKIE['phpPassSafe']);

$error['delete'] = false;

echo json_encode($error);

?>
