<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 26.06.16
 * Time: 20:24
 */

require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';


$logger = new Logger;
$logger->debug('EditPasswordDetails was called');
$show_updated_password = false;

if (isset($_COOKIE['phpPassSafe'])) {
    $user            = new Users;
    $loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
    die('you are not logged in');
}

$error            = array();
$filtered_input   = array();
$error_count      = 0;




if (ctype_digit($_GET['PasswordID'])){
    $filtered_input['PasswordID'] = $_GET['PasswordID'];
    $action = 'update';
} else {
    $filtered_input['PasswordID'] = '';
    $action = 'insert';
}

if (isset($_GET['PasswordGroupID']) && ctype_digit(trim($_GET['PasswordGroupID']))){
    $filtered_input['PasswordGroupID'] = trim($_GET['PasswordGroupID']);
} else {
    $error['PasswordGroupID'] = true;
    $error_count++;
}

$allowed_titel_characters = ALLOWED_CHARACTERS;
if (isset($_GET['Titel'])) {
    if (preg_match($allowed_titel_characters, $_GET['Titel'])){
        $filtered_input['Titel'] = $_GET['Titel'];
    } else {
        $error['Titel'] = true;
        $error_count++;
    }
} else {
    $filtered_input['Titel'] = '';
}

$allowed_name_characters = ALLOWED_CHARACTERS;
if (isset($_GET['UserName'])) {
    if (preg_match($allowed_name_characters, $_GET['UserName']) || trim($_GET['UserName']) == ''){
        $filtered_input['UserName'] = trim($_GET['UserName']);
    } else {
        $error['UserName'] = true;
        $error_count++;
    }
} else {
    $filtered_input['UserName'] = '';
}

$allowed_notes_characters = ALLOWED_CHARACTERS;
if (isset($_GET['Notes']) && $_GET['Notes'] !='') {
    if (preg_match($allowed_notes_characters, $_GET['Notes'])){
        $filtered_input['Notes'] = $_GET['Notes'];
    } else {
        $error['Notes'] = true;
        $error_count++;
    }
} else {
    $filtered_input['Notes'] = '';
}

if (isset($_GET['URL']) && trim($_GET['URL']) != '') {
    $allowed_url_characters = ALLOWED_EMAIL_CHARACTERS;
    if (preg_match($allowed_notes_characters, $_GET['URL'])){
        $filtered_input['URL'] = trim($_GET['URL']);
    } else {
        $error['URL'] = true;
        $error_count++;
    }
} else {
    $filtered_input['URL'] = '';
}

$allowed_password_characters = ALLOWED_CHARACTERS;
if (isset($_GET['Password']) && $_GET['Password'] != '') {
    if (preg_match($allowed_password_characters, $_GET['Password'])){
        $filtered_input['Password'] = $_GET['Password'];
    } else {
        $error['Password'] = true;
        $error_count++;
    }
} else {
    $error['Password'] = true;
    $error_count++;
}

if (isset($_GET['idoitID']) && ctype_digit(trim($_GET['idoitID']))) {
    $filtered_input['idoitID'] = trim($_GET['idoitID']);
} else {
    $filtered_input['idoitID'] = -1;
}



$updated_password = new Passwords;
$updated_password->PasswordGroupID = (isset($filtered_input['PasswordGroupID']))?$filtered_input['PasswordGroupID']:'';
$updated_password->PasswordID      = (isset($filtered_input['PasswordID']))?$filtered_input['PasswordID']:'';
$updated_password->Titel           = (isset($filtered_input['Titel']))?$filtered_input['Titel']:'';
$updated_password->UserName        = (isset($filtered_input['UserName']))?$filtered_input['UserName']:'';
$updated_password->Notes           = (isset($filtered_input['Notes']))?$filtered_input['Notes']:'';
$updated_password->URL             = (isset($filtered_input['URL']))?$filtered_input['URL']:'';
$updated_password->Password        = (isset($filtered_input['Password']))?$filtered_input['Password']:'';
$updated_password->idoitID         = (isset($filtered_input['idoitID']))?$filtered_input['idoitID']:-1;

if ($error_count == 0) {
    if ($action == 'update') {
        $updated_password->update($_COOKIE['phpPassSafe']);
    } elseif ($action == 'insert') {
        $logger->debug('inserting new password');
        $updated_password->insert($_COOKIE['phpPassSafe']);
    }

} else {
    $logger->debug('Error: ');
    $logger->debug(print_r($error, true));
}

echo json_encode($error);
