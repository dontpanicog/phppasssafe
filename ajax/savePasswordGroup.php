<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 29.03.17
 * Time: 12:08
 */


require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';


$logger = new Logger;
$logger->debug('savePasswordGroup was called');
$show_updated_password = false;

if (isset($_COOKIE['phpPassSafe'])) {
    $user            = new Users;
    $loggedin_user   = $user->check_credentials_from_cookie($_COOKIE['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
    die('you are not logged in');
}

$error            = array();
$filtered_input   = array();
$error_count      = 0;






if (isset($_GET['UserGroupID']) && ctype_digit(trim($_GET['UserGroupID']))){
    $filtered_input['UserGroupID'] = trim($_GET['UserGroupID']);
} else {
    $error['UserGroupID'] = true;
    $error_count++;
}

$allowed_titel_characters = ALLOWED_CHARACTERS;
if (isset($_GET['GroupTitel'])) {
    if (preg_match($allowed_titel_characters, $_GET['GroupTitel'])){
        $filtered_input['GroupTitel'] = $_GET['GroupTitel'];
    } else {
        $error['GroupTitel'] = true;
        $error_count++;
    }
} else {
    $filtered_input['GroupTitel'] = '';
}




$password_group = new PasswordGroups();
$password_group->UserGroupID       = (isset($filtered_input['UserGroupID']))?$filtered_input['UserGroupID']:'';
$password_group->PasswordGroupName = (isset($filtered_input['GroupTitel']))?$filtered_input['GroupTitel']:'';



if ($error_count == 0) {

    $logger->debug('inserting new password group');
    $password_group->add_group($_COOKIE['phpPassSafe']);

} else {
    $logger->debug(print_r($error, true));
}

echo json_encode($error);




?>
