<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 18.08.16
 * Time: 09:44
 */

require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/Passwords.php';
require_once '../model/PasswordGroups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../Smarty/Smarty.class.php';
require_once '../lang/' . LANGUAGE;

$validCharacters = array('-', '_', '.', '/', '\\');


$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
    $user = new Users;
    $loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    if (!$loggedin_user) die('you are not logged in');
} else {
    die ('');
}

$logger = new Logger();


$error            = false;
$errors           = array();
$filtered_input   = array(
    'first_name' => false,
    'last_name'  => false,
    'user_name'  => false,
    'email'      => false,
    'password'   => false,
    'password2'  => false
);

if (isset($_GET['user_id'])) {
    if (ctype_digit($_GET['user_id'])) {
        $filtered_input['user_id'] = $_GET['user_id'];
        $function_to_load = 'OnLoad="sndReq(\'\', \'\', ' . $filtered_input['user_id'] . ')"';
    }
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'save') {
        if (ctype_digit($_GET['user_id'])){
            $filtered_input['user_id'] = $_GET['user_id'];
            $action = 'update';
        } else {
            $filtered_input['user_id'] = '';
            $action = 'insert';
        }

        $allowed_name_characters = ALLOWED_CHARACTERS;

        if (isset($_GET['first_name'])) {
            if (preg_match($allowed_name_characters, $_GET['first_name']) || trim($_GET['first_name']) == ''){
                $filtered_input['first_name'] = trim($_GET['first_name']);
            } else {
                $errors['first_name'] = true;
            }
        } else {
            $filtered_input['first_name'] = '';
        }

        if (isset($_GET['last_name'])) {
            if (preg_match($allowed_name_characters, $_GET['last_name']) || trim($_GET['last_name']) == ''){
                $filtered_input['last_name'] = trim($_GET['last_name']);
            } else {
                $errors['last_name'] = true;
            }
        } else {
            $filtered_input['last_name'] = '';
        }

        if (ctype_alnum(str_replace($validCharacters, '', $_GET['user_name']))){
            $filtered_input['user_name'] = trim($_GET['user_name']);
        } else {
            $errors['user_name'] = true;
        }

        $allowed_email_characters = ALLOWED_EMAIL_CHARACTERS;
        if ($_GET['email']) {
            if (preg_match($allowed_email_characters, $_GET['email']) || trim($_GET['email']) == ''){
                $filtered_input['email'] = trim($_GET['email']);
            } else {
                $errors['email'] = true;
            }
        } else {
            $filtered_input['email'] = '';
        }

        $allowed_password_characters = ALLOWED_CHARACTERS;
        if (isset($_GET['password']) && $_GET['password'] != '') {
            if (preg_match($allowed_password_characters, $_GET['password'])){
                $filtered_input['password'] = trim($_GET['password']);
                if (preg_match($allowed_password_characters, $_GET['password2'])){
                    $filtered_input['password2'] = $_GET['password2'];
                    if ($filtered_input['password'] != $filtered_input['password2']) {
                        $errors['RepeatUserPassword'] = true;
                    }
                } else {
                    $errors['RepeatUserPassword'] = true;
                }
            } else {
                $errors['UserPassword'] = true;
            }
        } else {
            if ($action == 'insert') {
                $errors['UserPassword'] = true;
            }
        }



        if (isset ($_GET['active'])) {
            if($_GET['active'] == 'on') {
                $filtered_input['active'] = true;
            } else {
                $filtered_input['active'] = false;
            }
        } else {
            $filtered_input['active'] = true;
        }

        foreach ($errors as $item) {
            if ($item) $error = true;
        }

        if (!$error) {
            $updated_user = new Users;
            $updated_user->UserID       = $filtered_input['user_id'];
            $updated_user->FirstName    = $filtered_input['first_name'];
            $updated_user->LastName     = $filtered_input['last_name'];
            $updated_user->Name         = $filtered_input['user_name'];
            $updated_user->email        = $filtered_input['email'];
            $updated_user->UserPassword = (isset($filtered_input['password']))?$filtered_input['password']:'';
            $updated_user->active       = $filtered_input['active'];
            if ($action == 'update') {
                $updated_user->update($arguments['phpPassSafe']);
            } elseif ($action == 'insert') {
                $updated_user->insert($arguments['phpPassSafe']);
            }
        } else {
        }
    } else if ($_GET['action'] == 'delete') {
        if (ctype_digit($_GET['user_id'])){
            $filtered_input['user_id'] = $_GET['user_id'];
            $user_to_delete = new Users;
            $user_to_delete->UserID = $filtered_input['user_id'];
            $user_to_delete->delete();

        }
    }
}

echo json_encode($errors);
