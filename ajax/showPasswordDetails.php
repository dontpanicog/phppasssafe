<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../model/PasswordGroups.php';
require_once '../model/UserGroup_PasswordGroup_Mapping.php';
require_once '../model/Users_Groups_Mapping.php';
require_once '../model/Passwords.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../lang/' . LANGUAGE;

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user) die('you are not logged in');
} else {
	die ('no cookie value');
}

if (ctype_digit($_GET['password_id'])){
	$filtered_input['password_id'] = $_GET['password_id'];
} else {
	die('');
}


$password = Passwords::fetch_details_for_password_id($filtered_input['password_id'], $_COOKIE['phpPassSafe']);

$add_user  = new Users;
$add_user  = $add_user->fetch_details_for_user_id($password->AddUserID);
$edit_user = new Users;
$edit_user = $edit_user->fetch_details_for_user_id($password->EditUserID);
$add_time  = date('j.n.Y G:i:s', $password->AddTimestamp);
if ($password->EditTimestamp <= 1) {
	$edit_time = '';
} else {
	$edit_time = date('j.n.Y G:i:s', $password->EditTimestamp);
}
//var_dump($password);
$data = array(
    'PasswordID'      => $password->PasswordID,
	'Password'        => convert_to_utf_8($password->Password),
    'URL'             => convert_to_utf_8($password->URL),
    'UserName'        => convert_to_utf_8($password->UserName),
    'Titel'           => convert_to_utf_8($password->Titel),
    'Notes'           => convert_to_utf_8($password->Notes),
    'PasswordGroupID' => $password->PasswordGroupID,
	'idoitID'         => $password->idoitID
);

echo json_encode($data);


function convert_to_utf_8($string) {
	if (mb_detect_encoding($string) == 'ISO-8859-1') {
		return utf8_encode($string);
	} else {
		//return htmlentities($string, ENT_QUOTES, 'UTF-8');
		return $string;
	}
}
?>

