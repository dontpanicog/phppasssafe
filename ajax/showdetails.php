<?php
require_once '../Config.php';
require_once '../model/DB.php';
require_once '../model/Users.php';
require_once '../model/Groups.php';
require_once '../controller/Crypto.php';
require_once '../controller/Logger.php';
require_once '../Smarty/Smarty.class.php';

$url = str_replace('%20', '&', $_SERVER['QUERY_STRING']);
parse_str($url, $arguments);

if (isset($arguments['phpPassSafe'])) {
		$user = new Users;
		$loggedin_user   = $user->check_credentials_from_cookie($arguments['phpPassSafe']);
    	if (!$loggedin_user || $loggedin_user->UserID != ADMIN_UID) die('you are not logged in');
} else {
	die ('no cookie value');
}

$filtered_input = array();
if (ctype_alpha($_GET['details_type'])) $filtered_input['details_type'] = $_GET['details_type'];
if (ctype_digit($_GET['item_id']))      $filtered_input['item_id']      = $_GET['item_id'];

$groups = new Groups;
$users  = new Users;



if ($filtered_input['details_type'] == 'userdetails') {
	$details  = $users->fetch_details_for_user_id($filtered_input['item_id']);
	$realname = $details->FirstName ?  $details->FirstName . ' ' . $details->LastName : $details->LastName;
	$username = $details->Name;
	$email    = $details->email;

	$joined_groups = '';
	if ($details->JoinedGroups) {
		foreach ($details->JoinedGroups as $joined_group) {
			if ($joined_group['GroupName'] == $filtered_input['item_id']) continue;
			$joined_groups .= $joined_group['GroupName'] . ', ';
		}
	}

} elseif ($filtered_input['details_type'] == 'groupdetails') {
	$details = $users->fetch_details_for_user_id($filtered_input['item_id']);
}

?>
<table class="itemlist">
  <tr>
    <td style="vertical-align:top; width:100px;"><strong>Name:</strong></td>
    <td style="vertical-align:top; width:200px;"><?php echo htmlentities($realname . ' ',0 );?></td>
    <td style="vertical-align:top; width:80px;"><strong>Mitglied bei:</strong></td>
    <td rowspan="2" style="vertical-align:top;"><?php echo trim(htmlentities($joined_groups . ' ',0 ), ', ');?></td>
  </tr>
  <tr>
  	<td style="vertical-align:top;"><strong>Email:</strong></td>
  	<td style="vertical-align:top;"><?php echo htmlentities($email . ' ',0 );?></td>
  </tr>
  <tr>
  	<td style="vertical-align:top;"><strong>Anmeldename:</strong></td>
  	<td style="vertical-align:top;"><?php echo htmlentities($username . ' ',0 );?></td>
  </tr>
</table>


