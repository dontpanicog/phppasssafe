<?php

class Crypto {
	public  $text;
	public  $key; 
	public  $encrypted = array();
	

	
	function ssl_encrypt($text, $key) {

	    /* Generate Initialisation Vector */
	    $encrypted['iv'] = $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('AES-256-OFB'));

	    /* Encrypt data */
	    $encrypted['cypher'] = openssl_encrypt($text, 'AES-256-OFB', $key, 0, $encrypted['iv']);
	    
	    return $encrypted;
	}
		
	function ssl_decrypt($encrypted, $key) {
	    $decrypted = openssl_decrypt($encrypted['cypher'], 'AES-256-OFB',  $key, 0, $encrypted['iv']);

	    return $decrypted;
	}

    function encrypt($text, $key) {
        /* Open the cipher */
        $td = mcrypt_module_open('rijndael-256', '', 'ofb', '');

        /* Create the IV and determine the keysize length, use MCRYPT_RAND
         * on Windows instead */
        $encrypted['iv'] = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($key), 0, $ks);

        /* Intialize encryption */
        mcrypt_generic_init($td, $key, $encrypted['iv']);

        /* Encrypt data */
        $encrypted['cypher'] = mcrypt_generic($td, $text);

        /* Terminate encryption handler */
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return $encrypted;
    }

    function decrypt($encrypted, $key) {
        /* Open the cipher */
        $td = mcrypt_module_open('rijndael-256', '', 'ofb', '');

        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($key), 0, $ks);

        /* Initialize encryption module for decryption */
        mcrypt_generic_init($td, $key, $encrypted['iv']);

        /* Decrypt encrypted string */
        $decrypted = mdecrypt_generic($td, $encrypted['cypher']);

        /* Terminate decryption handle and close module */
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        /* Show string */
        //echo 'Decrypted: ' . trim($decrypted) . "\n";

        return $decrypted;
    }

	static function generate_key($length) {
    	$character_pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*+#-_!%&;:<>';
    	$key = '';
    	mt_srand ((double) microtime() * 1000000);
    	for ($i = 0; $i < $length; $i++) {
      		$key .= $character_pool{mt_rand (0,73)};
    	}
    	return $key;
	}
  	
  	static function calculate_hashed_password($salt, $hash_cyles, $password) {
  		$hashed_password = $password . $salt;
  		
  		for ($i = 1; $i <= $hash_cyles; $i++) {
  			$hashed_password = sha1($hashed_password);
  		}
  		
  		return $hashed_password;
  	}
	
}
?>
