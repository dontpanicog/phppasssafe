<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 20.08.16
 * Time: 16:25
 */
class Init {
    public function __construct() {
        $table = new Groups();
        $table->create_table();

        $table = new PasswordGroups();
        $table->create_table();

        $table = new Passwords();
        $table->create_table();

        $table = new Users();
        $table->create_table();

        $table = new Users_Groups_Mapping();
        $table->create_table();


        $admin = new Users;

        $admin->Name = 'admin';
        $admin->UserPassword = 'changeme';
        $admin->active = true;

        $admin_id = $admin->create_admin();

        return $admin_id;
    }

}
