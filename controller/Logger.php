<?php
class Logger {
	public function debug($message){
		$line  = date('Y-m-d H:i:s') . ': ';
		$line .= $message . "\n";
		
		$debuglog = fopen(DEBUGLOG, 'a');
		if (!$debuglog) die('unable to open debuglog');
		if (DEBUGGING) {
			fwrite($debuglog, $line);
			fclose($debuglog);
		}
	}
	
	public function access($message){
		$accesslog = fopen(LOGDIR . date('Ymd') . 'accsess.log', 'a');
		$line  = date('Y-m-d H:i:s') . ': ';
		$line .= $message . "\n";
		if (!$accesslog) die('unable to open accesslog');
		fwrite($accesslog, $line);
		fclose($accesslog);
	}
}