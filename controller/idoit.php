<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 26.09.16
 * Time: 16:06
 */
class idoit
{
    public $apikey;
    public $apiurl;
    public $header;
    public $content;

    function __construct() {
    }

    function call() {

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => $this->header,
                'content' => json_encode($this->content)
            )
        ));

        //var_dump($this->header);

        $response = json_decode(file_get_contents($this->apiurl, FALSE, $context));
        return $response;

    }


    function receive_objects($search) {
        $this->header = "Content-Type: application/json\r\n";
        $this->content =  array(
            'method' => 'cmdb.objects',
            'params' => array(
                'apikey' => $this->apikey,
                'filter' => array(
                    'title' => '%' . $search . '%'
                )
            ),
            'id' => 1,
            'version' => "2.0"
        );

        $ojbs = $this->call();

        return $ojbs->result;
    }

}
