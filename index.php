<?php
/*
 * Login Seite
 */
require_once 'Config.php';
require_once 'model/DB.php';
require_once 'model/Users.php';
require_once 'model/Groups.php';
require_once 'model/PasswordGroups.php';
require_once 'model/Passwords.php';
require_once 'model/Users_Groups_Mapping.php';
require_once 'controller/Crypto.php';
require_once 'controller/Logger.php';
require_once 'Smarty/Smarty.class.php';
require_once 'lang/' . LANGUAGE;

$validCharacters = array('-', '_', '.', '/', '\\');


if (!Users::admin_exists()) {
	setcookie("phpPassSafe", '', -9999, '', '', 0);
	require_once 'controller/Init.php';
	new Init();
}

$logger        = new Logger;
$loggedin_user = new Users;

if (isset($_REQUEST['action'])) {
	$filtered_input = array();
	if (ctype_alpha(str_replace($validCharacters, '', $_REQUEST['action'])))   $filtered_input['action']   = $_REQUEST['action'];


	if ($filtered_input['action'] != 'logout') {
		if (ctype_alnum(str_replace($validCharacters, '', $_REQUEST['name'])))     $filtered_input['name']     = $_REQUEST['name'];

		$allowed_password_characters = ALLOWED_CHARACTERS;
		if (isset($_POST['password']) && $_POST['password'] != '') {
			if (preg_match($allowed_password_characters, $_POST['password'])){
				$filtered_input['password'] = trim($_POST['password']);
			}
		}

		if (isset($filtered_input['name']) && isset($filtered_input['password'])) {
			if ($loggedin_user->check_credentials_from_login($filtered_input['name'], $filtered_input['password'])) {
				$logger->debug($filtered_input['name'] . ' logged in' . "\n");
				if ($filtered_input['name'] == 'admin') {
					header("location: UserManagement.php");
				} else {
                    header("location: PasswordManagement.php");

				}
			} else {
				$logger->debug('Illegal login attempt for user ' . $filtered_input['name'] . "\n");
			}
		} else {
			$logger->debug('username or password empty' . "\n");
		}
	}elseif ($filtered_input['action'] == 'logout' && isset($_COOKIE['phpPassSafe'])) {
		if ($loggedin_user = $loggedin_user->check_credentials_from_cookie($cookie_value = $_COOKIE['phpPassSafe'])) $loggedin_user->logout();
	}
} elseif (isset($_COOKIE['phpPassSafe'])) {
    if ($loggedin_user = $loggedin_user->check_credentials_from_cookie($cookie_value = $_COOKIE['phpPassSafe'])) {
    	if ($loggedin_user->Name == 'admin') {
			header("location: UserManagement.php");
		} else {
			header("location: PasswordManagement.php");
		}
    }
}






$smarty = new Smarty;

$smarty->assign('login', LOGIN);
$smarty->assign('username', USERNAME);
$smarty->assign('password', PASSWORD);
$smarty->display('login.html');
?>
