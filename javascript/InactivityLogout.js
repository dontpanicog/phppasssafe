var IntervalCounter = 0;
var active          = 0; 

$(document).ready(function () {
	var inactivity_timeout = $('#inactivity_timeout').val();
	InactivityLogout(inactivity_timeout);


});


function InactivityLogout(inactivity_timeout) {
	if (!active) {
		active = setInterval("InactivityCounter()", inactivity_timeout);
	}
}

function InactivityCounter() {
	IntervalCounter = IntervalCounter + 1;
	if (IntervalCounter >= 1000) {
		clearInterval(active);
		window.location ='index.php?action=logout';
	}
}

function ResetCounter() {
	IntervalCounter = 0;
}
