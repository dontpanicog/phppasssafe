var resObject;

if(navigator.appName.search("Microsoft") > -1) {
  resObject = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObject = new XMLHttpRequest();
}

$('#Go').click(function () {

});

$('#RealyDeletePassword').click(function () {
    var PasswordID         = $('#PasswordID').val();
    $.getJSON('ajax/deletePassword.php?PasswordID='+PasswordID + '&' + document.cookie, function(error){
        if (error.delete) {
            // Todo: display error?
        } else {
            fetch_passwords();
        }
    });
});


$('#SavePassword').click(function () {
    var Titel              = encodeURIComponent($('#Titel').val());
    var UserName           = encodeURIComponent($('#UserName').val());
    var Password           = encodeURIComponent($('#Password').val());
    var PasswordID         = encodeURIComponent($('#PasswordID').val());
    var PasswordGroupID    = encodeURIComponent($('#PasswordGroupID').val());
    var URL                = encodeURIComponent($('#URL').val());
    var Notes              = encodeURIComponent($('#Notes').val());
    var idoitID            = encodeURIComponent($('#IdoitObject').val());


    $.getJSON('ajax/savePassword.php?PasswordID='+PasswordID + '&Titel=' + Titel + '&UserName=' +  UserName + '&Password=' + Password + '&PasswordGroupID= ' + PasswordGroupID + '&URL=' + URL + '&idoitID= ' + idoitID + '&Notes=' + Notes + '&' + document.cookie,
        function(error){
            var error_occured = false;

            if (error.Title) {
                $('#Titel').next("i").removeClass('hidden');
                $('#Titel').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error.Password) {
                $('#Password').next("i").removeClass('hidden');
                $('.Password').addClass('has-error');
                error_occured = true;
            }

            if (error.PasswordGroupID) {
                $('#PasswordGroupID').next("i").removeClass('hidden');
                $('#PasswordGroupID').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error.Notes) {
                $('#Notes').next("i").removeClass('hidden');
                $('#Notes').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error.URL) {
                $('#URL').next("i").removeClass('hidden');
                $('#URL').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error_occured) {
                // nothing to do
            } else {
                fetch_passwords();
                $("#PasswordDetails").modal('toggle');
            }
        }
    );
});


$('#SavePasswordGroup').click(function () {

    var GroupTitel         = encodeURIComponent($('#GroupTitel').val());
    var UserGroupID        = encodeURIComponent($('#UserGroupID').val());


    $.getJSON('ajax/savePasswordGroup.php?GroupTitel='+GroupTitel + '&UserGroupID=' + UserGroupID + '&' + document.cookie,
        function(error){
            var error_occured = false;

            if (error.GroupTitel) {
                $('#GroupTitel').next("i").removeClass('hidden');
                $('#GroupTitel').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error.UserGroupID) {
                $('#UserGroupID').next("i").removeClass('hidden');
                $('#UserGroupID').parent("div").addClass('has-error');
                error_occured = true;
            }

            if (error_occured) {
                // nothing to do
            } else {
                fetch_passwords();
                $("#PasswordGroupDetails").modal('toggle');
            }
        }
    );
});


$(".glyphicon-plus").click(function(){
    $("#PasswordID").val('');
    $("#Password").val('');
    $("#Titel").val('');
    $("#UserName").val('');
    $("#URL").val('');
    $("#Notes").val('');
    $("#IdoitObject").val('');
    $("#URL1").attr("");

    var selected_password_group_item = $('.PasswordGroup_item.active').attr('id');
    var selected_password_group_id   = selected_password_group_item.substring(14);

    $("#PasswordGroupID").val(selected_password_group_id);

    $(".url").hide();
    $("#URL").show();


});

var timeoutVar;

function fetch_password_details(password_id) {
    $("#PasswordID").val('');
    $("#Password").val('');
    $("#Titel").val('');
    $("#UserName").val('');
    $("#URL").val('');
    $("#Notes").val('');
    $("#IdoitObject").val('');
    $("#URL1").attr("");

    $.getJSON('ajax/showPasswordDetails.php?password_id='+ password_id + '&' + document.cookie,
        function(data){
            $("#PasswordID").val(data.PasswordID);
            $("#Password").val(data.Password);
            $("#Titel").val(data.Titel);
            $("#UserName").val(data.UserName);
            $("#URL").val(data.URL);
            $("#URL1").attr("href", data.URL);
            $("#URL1").text(data.URL);
            $("#Notes").val(data.Notes);
            $("#PasswordGroupID").val(data.PasswordGroupID);
            $("#IdoitObject").val(data.idoitID);

            if (data.URL) {
                $("#URL").hide();
                $(".url").show();
            } else {
                $(".url").hide();
                $("#URL").show();
            }
        }
    );

    $('#PasswordDetails').on('shown.bs.modal', function () {
        $('.chosen-select', this).chosen();
    });

    timeoutVar = setTimeout("clear()", $("#displaytime").val());
}

$('#PasswordDetails').on('hidden.bs.modal', function () {
    clearTimeout(timeoutVar);
});


function sndDetailReq(password_id, displaytimeout) {
	if (!document.cookie) window.location ='index.php';
    resObject.open('get','ajax/showPasswordDetails.php?password_id='+password_id + '&' + document.cookie ,true);
    resObject.onreadystatechange = handleResponse;
    resObject.send(null);
    setTimeout("clear()", displaytimeout);
}

function handleResponse() {
  if(resObject.readyState == 4){
      document.getElementById("PasswordDetails").innerHTML = resObject.responseText;
  }
}

$("#URLedit").click(function () {
    $(".url").hide();
    $("#URL").show();
});


$(".glyphicon-eye-open").on("click", function() {
    $(this).toggleClass("glyphicon-eye-close");
    var type = $("#Password").attr("type");
    if (type == "text") {
        $("#Password").attr("type", "password");
    } else {
        $("#Password").attr("type", "text");
        $("#Password").select();
    }
});


$(".btn-close").on("click", function() {
  var type = $("#Password").attr("type");
  if (type == "text") {
  { $("#Password").attr("type", "password");}
}
});

/*

function copyToClopboard() {
  var type = $("#Password").attr("type");
  if (type == "password") {
    { $("#Password").attr("type", "text");}
    var hidden = true;
  }
  var copyText = document.getElementById("Password");
  copyText.select();
  document.execCommand("copy");
  if (hidden) {
    { $("#Password").attr("type", "password");}
  }
}

*/

function clear() {

    $("#PasswordID").val('');
    $("#Password").val('');
    $("#Titel").val('');
    $("#UserName").val('');
    $("#URL").val('');
    $("#Notes").val('');
    $("#IdoitObject").val('');
    $("#URL1").attr("");
    $("#PasswordDetails").modal('hide');

}

$("#selected_password").focus(function(){
	setTimeout(function(){$(this).select();},10);
});

$(document).ready(function () {
  //  $(".chosen-select").chosen();

});


$(".edit_group").click(function(){
    var selected_password_group_id = $('.PasswordGroup_item.active').attr('id');
    fetch_password_group_details(selected_password_group_id);
});


function fetch_password_group_details(password_group_id) {
    $("#PasswordGroupID").val('');
    $("#UserGroupID").val('');

    $.getJSON('ajax/showPasswordGroupDetails.php?password_group_id='+ password_group_id + '&' + document.cookie,
        function(data){
            $("#PasswordGroupID").val(data.PasswordGroupID);
            $("#PasswordGroupName").val(data.PasswordGroupName);
            $("#UserGroupID").val(data.UserGroupID);
            $("#UserGroupName").val(data.UserGroupName);
        }
    );

}
