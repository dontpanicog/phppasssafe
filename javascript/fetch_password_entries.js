var resObjectUser;
var resObjectGroup;

if(navigator.appName.search("Microsoft") > -1) {
  resObjectUser = new ActiveXObject("MSXML2.XMLHTTP");
  resObjectGroup = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObjectUser = new XMLHttpRequest();
  resObjectGroup = new XMLHttpRequest();
}

function sndPasswordGroupReq() {
	if (!document.cookie) window.location ='index.php';
    resObjectGroup.open('get','ajax/DisplayPasswordGroups.php?' + document.cookie,true);
    resObjectGroup.onreadystatechange = handleGroupResponse;
    resObjectGroup.send(null);
}

function handleGroupResponse() {
  if(resObjectGroup.readyState == 4){
      document.getElementById("GroupList").innerHTML = resObjectGroup.responseText;
  }
}

function submitSearchForm(password_group_id) {
    $('#password_group_id').val(password_group_id);
    document.search_form.submit();
}

function sndPasswordReq(group_id) {
    fetch_passwords();
}

function handlePasswordResponse() {
if(resObjectUser.readyState == 4){
    document.getElementById("PasswordList").innerHTML = resObjectUser.responseText;
}
}

function repeatLoop() {
	sndPasswordReq();
	sndPasswordGroupReq();
}

function repeatGroupLoop() {
	sndGroupReq();
	setTimeout("repeatGroupLoop()", 12300);
}

function confirmDelete(group_id, confirm_text) {
	if (confirm(confirm_text)) {
		sndGroupDelReq(group_id);
	}
}

function sndGroupDelReq(group_id) {
	if (!document.cookie) window.location ='index.php';
    resObjectGroup.open('get','ajax/DisplayPasswordGroups.php?action=delete&group_id='+ group_id + '&' + document.cookie ,true);
    resObjectGroup.onreadystatechange = handleGroupResponse;
    resObjectGroup.send(null);
}

function newPassword() {
	group_id = document.getElementById("preselectedGroup").value;
	openwindow('EditPasswordDetails.php?preselct_group_id=' + group_id);
}

$(document).ready(function(){
    fetch_passwords();

});

function fetch_passwords(){
    var p =  $("#page").val();
    var password_group_id =  $("#password_group_id").val();
    var titel =  $("#search").val();


    $.getJSON('ajax/DisplayPasswords.php?p='+ p + '&titel=' + titel + '&password_group_id=' + password_group_id + '&' + document.cookie,
        function(data){
            $('#PasswordTable > tbody').empty();
            $.each(data.passwords, function (index, password) {
                $('#PasswordTable > tbody:last-child').append('' +
                    '<tr class="password-row" data-toggle="modal" data-target="#PasswordDetails" id="PasswordID_' + password.PasswordID + '" onclick="fetch_password_details(' + password.PasswordID + ')"> ' +
                        '<td colspan="2">' + password.Titel + '</td>' +
                        '<td class="hidden-xs">' + password.PasswordGroupName + '</td>' +
                        '<td>' + password.UserName + '</td>' +
                        '<td class="hidden-xs">' + password.Notes + '</td> ' +
                    '</tr>')

            });

            //$("#pages").val(data.count);

            var pages = data.count;
            if (pages < 7) {
                var visible_pages = pages;
            } else {
                var visible_pages = 7;
            }

            //console.log(pages);

            $('#pagination').twbsPagination({
                totalPages: pages,
                visiblePages: visible_pages,
                onPageClick: function (event, page) {
                    $("#page").val(page);

                    fetch_passwords();
                }
            });

        }
    );
}



function select_password_group(PasswordGroupID) {
    $("#password_group_id").val(PasswordGroupID);
    $('.PasswordGroup_item.active').removeClass('active');
    $('#PasswordGroup_' + PasswordGroupID).addClass('active');
    fetch_passwords();
}



