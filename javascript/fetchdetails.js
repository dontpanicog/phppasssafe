var resObject;

if(navigator.appName.search("Microsoft") > -1) {
  resObject = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObject = new XMLHttpRequest();
}

function sndReq(details_type, item_id) {
	if (!document.cookie) window.location ='index.php';
    resObject.open('get','ajax/showdetails.php?details_type='+details_type+'&item_id='+item_id + '&' + document.cookie,true);
    resObject.onreadystatechange = handleResponse;
    resObject.send(null);
}

function handleResponse() {
  if(resObject.readyState == 4){
      document.getElementById("itemdetails").innerHTML = resObject.responseText;
  }
}
