var resObjectUser;
var resObjectGroup;

if(navigator.appName.search("Microsoft") > -1) {
  resObjectUser = new ActiveXObject("MSXML2.XMLHTTP");
  resObjectGroup = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObjectUser = new XMLHttpRequest();
  resObjectGroup = new XMLHttpRequest();
}

function sndGroupReq() {
	if (!document.cookie) window.location ='index.php';
    resObjectGroup.open('get','ajax/DisplayGroups.php?' + document.cookie,true);
    resObjectGroup.onreadystatechange = handleGroupResponse;
    resObjectGroup.send(null);
}

function handleGroupResponse() {
  if(resObjectGroup.readyState == 4){
      document.getElementById("GroupList").innerHTML = resObjectGroup.responseText;
  }
}

function sndUserReq(group_id) {
	if (!document.cookie) window.location ='index.php';
	var name         = document.getElementById("filter");
    resObjectUser.open('get','ajax/DisplayUsers.php?group_id='+ group_id + '&name='+ name.value + '&' + document.cookie,true);
    resObjectUser.onreadystatechange = handleUserResponse;
    resObjectUser.send(null);
}

function handleUserResponse() {
if(resObjectUser.readyState == 4){
    document.getElementById("UserList").innerHTML = resObjectUser.responseText;
}
}

function repeatLoop() {
	sndUserReq();
	sndGroupReq();
	//setTimeout("repeatLoop()", 10000);
}

function repeatGroupLoop() {
	sndGroupReq();
	setTimeout("repeatGroupLoop()", 12300);
}

function confirmDelete(group_id, confirm_text) {
	if (confirm(confirm_text)) {
		sndGroupDelReq(group_id);
	}
}

function sndGroupDelReq(group_id) {
	if (!document.cookie) window.location ='index.php';
    resObjectGroup.open('get','ajax/DisplayGroups.php?action=delete&group_id='+ group_id + '&' + document.cookie ,true);
    resObjectGroup.onreadystatechange = handleGroupResponse;
    resObjectGroup.send(null);
}


