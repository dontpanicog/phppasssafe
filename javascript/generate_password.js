/*function AcceptPassword() {
	opener.document.user_details.password.value=document.password_generation.generated_password.value;
	opener.document.user_details.password2.value=document.password_generation.generated_password.value;
	window.close();
}*/


$('#accept-suggestion').click(function () {
    $('#Password').val($('#password-suggestion').val());
});

function GeneratePassword() {
	// ToDo: Umstellung auf jquery syntax

    if (parseInt(navigator.appVersion) <= 3) {
        alert("Sorry this only works in 4.0+ browsers");
        return true;
    }

	var prefix             = $('#prefix').val();
    var Password           = prefix;
    var length             = $('#password-length').val();

    var upper_case         = $('#upper-case-characters').is(':checked');
    var lower_case         = $('#lower-case-characters').is(':checked');
    var digits             = $('#digit-characters').is(':checked');
    var special_characters = $('#special-characters').is(':checked');

    if (upper_case || lower_case || digits || special_characters) { 

	    for (i=0; i < length; i++) {
	
	        num = getRandomNumber();
	        
	        
	        while (CheckCharacters(upper_case, lower_case, digits, special_characters, num)) { num = getRandomNumber(); } 
	
	        Password = Password + String.fromCharCode(num);
	    }
	
	    $('#password-suggestion').val(Password);
    }

    return true;
}

function getRandomNumber() {

    // between 0 - 1
    var rndNum = Math.random();

    // rndNum from 0 - 1000
    rndNum = parseInt(rndNum * 1000);

    // rndNum from 33 - 127
    rndNum = (rndNum % 94) + 33;
   

    return rndNum;
}


function CheckCharacters(upper_case_allowed, lower_case_allowed, digits_allowed, special_characters_allowed, num) { // returns true if character does NOT match the chosen criteria
	var special_character = false;
	var digit             = false;
	var lower_case        = false;
	var upper_case        = false;
	

    
    if ((num >=48) && (num <=57))   { digit      = true; } // digits 
	if ((num >=97) && (num <=122))  { lower_case = true; } // lowercase
	if ((num >=65) && (num <=90))   { upper_case = true; }  // uppercase	

    if (	((num >=33) && (num <=47)) ||  // special
    		((num >=58) && (num <=64)) || 
    		((num >=91) && (num <=96)) || 
    		((num >=123)&& (num <=126))
    	) { special_character = true; }

	
	if (special_character || digit || lower_case || upper_case) {
		if (upper_case_allowed && upper_case)                return false; 
		if (lower_case_allowed && lower_case)                return false;
		if (digits_allowed && digit)                         return false; 
		if (special_characters_allowed && special_character) return false;
	} else {
		return true;
	}

	return true;

}
