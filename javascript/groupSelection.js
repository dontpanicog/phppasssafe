var resObject;

if(navigator.appName.search("Microsoft") > -1) {
  resObject = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObject = new XMLHttpRequest();
}

function sndReq(direction, group_id, user_id) {
    resObject.open('get','ajax/GroupSelection.php?direction='+direction+'&group_id='+group_id+'&user_id='+user_id + '&' + document.cookie,true);
    resObject.onreadystatechange = handleResponse;
    resObject.send(null);
}

function handleResponse() {
	if(resObject.readyState == 4){
	    document.getElementById("GroupSelection").innerHTML = resObject.responseText;
	}
}