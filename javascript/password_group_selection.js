var resObject;

if(navigator.appName.search("Microsoft") > -1) {
  resObject = new ActiveXObject("MSXML2.XMLHTTP");
}
else {
  resObject = new XMLHttpRequest();
}

function sndReq(direction, user_group_id, password_group_id) {
    resObject.open('get','ajax/PasswordGroupSelection.php?direction='+direction+'&user_group_id='+user_group_id+'&password_group_id='+password_group_id + '&' + document.cookie,true);
    resObject.onreadystatechange = handleResponse;
    resObject.send(null);
}

function handleResponse() {
	if(resObject.readyState == 4){
	    document.getElementById("GroupSelection").innerHTML = resObject.responseText;
	}
}