/**
 * Created by chris on 18.08.16.
 */

$('#SaveUserSettings').click(function(){
    var action = 'save';
    var user_id = $("#UserID").val();
    var first_name = encodeURIComponent($("#first_name").val());
    var last_name = encodeURIComponent($("#last_name").val());
    var user_name = encodeURIComponent($("#user_name").val());
    var email = encodeURIComponent($("#email").val());
    var password = encodeURIComponent($("#UserPassword").val());
    var password2 = encodeURIComponent($("#RepeatUserPassword").val());

    var has_error = false;

    $.getJSON('ajax/saveUserDetails.php?action='+ action + '&user_id=' + user_id + '&first_name=' + first_name + '&last_name=' + last_name + '&user_name=' + user_name + '&email=' + email + '&password=' + password + '&password2=' + password2 + '&' + document.cookie,
        function(error){
            $.each(error, function (index, value) {
                $('#fg_' + index).addClass('has-error');
                has_error = true;
            });

            if (!has_error) {
                $('#UserSettings').modal('hide');
            }
        }
    );

});
