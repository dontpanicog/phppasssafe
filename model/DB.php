<?php

class DB {
	private $dbhost   = DB_HOST; //where the DB runs
	private $db       = DB; //name of the db
	private $dbuser   = DB_USER; //the db user
	private $dbpasswd = DB_PASSWORD; //db password
	public  $linkID;
/*
	public function __construct() {
	  	$this->linkID = mssql_connect($this->dbhost, $this->dbuser, $this->dbpasswd);
		if (!$this->linkID) die('DB connection failed!');
		if (mssql_select_db($this->db, $this->linkID)) {
			echo '';
		} else {
			die('mssql_select failed' . $this->db . ', ' . $this->linkID);
		}
	}

	public function execute($query) {
		$resID = mssql_query($query, $this->linkID);
		if (!$resID) die ("Query Error: $query.");
		return $resID;
	}
*/
	public function execute($sql) {
		try {
			if (DB_SYSTEM == 'sqlsrv') {
				$dsn = DB_SYSTEM . ':server=' . DB_HOST . ';Database =' . DB; // sehr bloed, mit neuen Treibern von MS haben sich die Schluesselwoerter geaendert
			} else {
				$dsn = DB_SYSTEM . ':host=' . DB_HOST . ';dbname=' . DB;
			}
			$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
	        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        $dbh->exec($sql);
	        return true;
		} catch (PDOException $e){
			$logger = new Logger;
			$logger->debug('Failed: ' . $e->getMessage());
			$logger->debug($sql);
			return false;
		}
	}

	public function query($sql) {
		$logger = new Logger;
		try {
		if (DB_SYSTEM == 'sqlsrv') {
				$dsn = DB_SYSTEM . ':server=' . DB_HOST . ';Database =' . DB; // sehr bloed, mit neuen Treibern von MS haben sich die Schluesselwoerter geaendert
			} else {
				$dsn = DB_SYSTEM . ':host=' . DB_HOST . ';dbname=' . DB;
			}
			$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
	        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        $results = $dbh->query($sql);
	        return $results;
		} catch (PDOException $e){
			$logger = new Logger;
			$logger->debug('Failed: ' . $e->getMessage());
			return false;
		}
	}
}
?>
