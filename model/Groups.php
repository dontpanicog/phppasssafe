<?php

class Groups {
	public $GroupID;
	public $GroupName;
	public $UserContainer;

	public function fetch_all_groups($without_container_groups=false){
		$db    = new DB;
		if ($without_container_groups) {
			$query = 'select * from Groups where UserContainer=0 order by GroupName asc';
		} else {
			$query = 'select * from Groups order by GroupName asc';
		}

		$results = $db->query($query);
		$groups = array();
		foreach ($results as $result) {
			$item                   = new Groups;
			$item->GroupID          = $result['GroupID'];
			$item->GroupName        = $result['GroupName'];
			$item->UserContainer    = $result['UserContainer'];
			$groups[$item->GroupID] = $item;
		}
		return $groups;
	}

	public function fetch_details_for_group($group_id){
		$db    = new DB;
		$query = 'select * from Groups where GroupID=\'' . $group_id . '\'';

		$results = $db->query($query);
		$groups = array();
		foreach ($results as $result) {
			$item                       = new Groups;
			$item->GroupID              = $result['GroupID'];
			$item->GroupName            = $result['GroupName'];
			$item->UserContainer        = $result['UserContainer'];
			$item->AdminCryptedGroupKey = unserialize(base64_decode($result['AdminCryptedGroupKey']));
		}
		return $item;
	}

	public function fetch_group_id_for_group_name($group_name) {
		$db = new DB;
		$query = 'select GroupID from Groups where GroupName=\'' . $group_name . '\'';
		$results = $db->query($query);

		foreach ($results as $result) {
			$group_id = $result['GroupID'];
		}
		if ($group_id) {
			return $group_id;
		} else {
			return -1;
		}
	}

	public function add_group($cookie_value) {
		$crypto = new Crypto;
		$users  = new Users;
		$logger = new Logger;
		$logger->debug('adding new user group ' . $this->GroupName);

		$loggedin_user    = $users->check_credentials_from_cookie($cookie_value);
		$logger->debug('by user ' . $loggedin_user->Name);
		$crypted_user_key = unserialize(base64_decode($loggedin_user->UserCryptedUserKey));
		$user_password    = Users::get_password_from_cookie($cookie_value);
		$user_key         = $crypto->decrypt($crypted_user_key, $user_password);
		$logger->debug('User Key: ' . $user_key);
		$group_key         = $crypto->generate_key(20);
		$logger->debug('Group Key: ' . $group_key);
		$crypted_group_key = base64_encode(serialize($crypto->encrypt($group_key, $user_key)));


		if (!$this->UserContainer) $this->UserContainer = 0;
		$db    = new DB;
		$query = 'insert into Groups (GroupName, UserContainer, AdminCryptedGroupKey) values (\'' . $this->GroupName . '\', ' . $this->UserContainer . ', \'' . $crypted_group_key . '\')';

		$db->execute($query);
	}

	public function delete() {
		//if (Groups::is_populated()) die('Gruppe ist nicht leer');

        $password_groups = new PasswordGroups();
		if ($password_groups->association_exists($this->GroupID)) {
		    $password_group = PasswordGroups::fetch_details_for_password_group_name($this->GroupName);
            $password_group->delete();
        }
		$db    = new DB;
        $query = 'delete from Users_Groups_Mapping where GroupID=' . $this->GroupID;
        $db->execute($query);
		$query = 'delete from Groups where GroupID=' . $this->GroupID;
		$db->execute($query);
	}

	public function fetch_admin_crypted_group_key_for_user_id($user_group_id){
		$db     = new DB;

		$query  = 'select * from Groups where GroupID=\'' . $user_group_id . '\'';
		$results = $db->query($query);
		foreach ($results as $result) {
			$storeable_admin_crypted_group_key  = $result['AdminCryptedGroupKey'];
			$serialized_admin_crypted_group_key = base64_decode($storeable_admin_crypted_group_key);
			$admin_crypted_group_key            = unserialize($serialized_admin_crypted_group_key);
		}
		return $admin_crypted_group_key;
	}

	private function is_populated(){
		$db = new DB;
		$query =  'select count(*) from Users_Groups_Mapping where GroupID = ' . $this->GroupID;
		$results =  $db->query($query);
		foreach ($results as $result) {
			if ($result[0] > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function create_table() {
	    $db = new DB;
        $query = '
CREATE TABLE IF NOT EXISTS `' . DB . '`.`Groups` (
  `GroupID` INT(11) NOT NULL AUTO_INCREMENT,
  `GroupName` VARCHAR(50) NOT NULL,
  `UserContainer` TINYINT(1) NOT NULL,
  `AdminCryptedGroupKey` TEXT NOT NULL,
  PRIMARY KEY (`GroupID`))
ENGINE = MyISAM
AUTO_INCREMENT = 128
DEFAULT CHARACTER SET = utf8
';
        $db->execute($query);
        
    }

}
