<?php
class PasswordGroups {
	public $PasswordGroupID;
	public $UserGroupID;
	public $PasswordGroupName;
	public $AdminCryptedPasswordGroupKey;
	public $UserGroupCryptedPasswordGroupKey;
	public $private=false;

	public function add_group($cookie_value) {
		$crypto = new Crypto;
		$users  = new Users;
		$groups = new Groups;
		$logger = new Logger;

		$logger->debug('new passwordgroup ' . $this->PasswordGroupName);
        $logger->debug('for user group id ' . $this->UserGroupID);

		$loggedin_user    = $users->check_credentials_from_cookie($cookie_value);
		$crypted_user_key = unserialize(base64_decode($loggedin_user->UserCryptedUserKey));
		$admin_password   = Users::get_password_from_cookie($cookie_value);
		$admin_key        = $crypto->decrypt($crypted_user_key, $admin_password);

		//$selected_user_group              = $groups->fetch_details_for_group($user_group_id);
        $logger->debug('UserGroup ID: ' . $this->UserGroupID);
		$admin_crypted_user_group_key     = $groups->fetch_admin_crypted_group_key_for_user_id($this->UserGroupID);
		$user_group_key                   = $crypto->decrypt($admin_crypted_user_group_key, $admin_key);

		$password_group_key         = $crypto->generate_key(20);
		$logger->debug('admin key: ' . $admin_key);
		$logger->debug('user group key: ' . $user_group_key);
		$logger->debug('password group key: ' . $password_group_key);

		$admin_crypted_password_group_key      = base64_encode(serialize($crypto->encrypt($password_group_key, $admin_key)));
		$user_group_crypted_password_group_key = base64_encode(serialize($crypto->encrypt($password_group_key, $user_group_key)));

		$db    = new DB;
		$query = 'insert into PasswordGroups (UserGroupID, PasswordGroupName, AdminCryptedPasswordGroupKey, UGroupCryptedPGroupKey, private) values (' .
			$this->UserGroupID . ', ' .
			'\'' . $this->PasswordGroupName . '\', ' .
			'\'' . $admin_crypted_password_group_key . '\', ' .
			'\'' . $user_group_crypted_password_group_key . '\', ' .
			'\'' . $this->private . '\' ' .
		')';
		$logger->debug($query);
		$db->execute($query);
	}

	public function update($cookie_value) {
		$crypto = new Crypto;
		$users  = new Users;
		$groups = new Groups;

		$db    = new DB;
		$query = 'update PasswordGroups set PasswordGroupName = \'' . $this->PasswordGroupName . '\' where PasswordGroupID = ' . $this->PasswordGroupID;

		$db->execute($query);
	}

	public static function fetch_all_groups($user_id, $fetch_private_groups=false){
		$logger = new Logger;
		$db     = new DB;
		$user   = new Users;
		$user   = $user->fetch_details_for_user_id($user_id);

		$joined_user_group_ids = '';
		foreach ($user->JoinedGroups as $usergroup_id => $usergroup) {
			$joined_user_group_ids .= $usergroup_id . ',';
		}
		$joined_user_group_ids = trim($joined_user_group_ids, ',');

		$and_private = ($fetch_private_groups == false)? ' and private = \'false\' ': ' ';

		if ($user_id == ADMIN_UID) {
			$query = 'select * from PasswordGroups where 1 = 1' . $and_private . 'order by PasswordGroupName asc';
			$logger->debug($query);
		}else if ($joined_user_group_ids != ''){
			$query = 'select * from PasswordGroups where 1 = 1' . $and_private . 'and UserGroupID in (' . $joined_user_group_ids . ') order by PasswordGroupName asc';
			$logger->debug($query);
		} else {
			return false;
		}

		$results = $db->query($query);
		$password_groups = array();

		foreach ($results as $result) {
			$item = new PasswordGroups;
			$item->PasswordGroupID              = $result['PasswordGroupID'];
			$item->UserGroupID                  = $result['UserGroupID'];
			$item->PasswordGroupName            = $result['PasswordGroupName'];
			$item->AdminCryptedPasswordGroupKey = $result['AdminCryptedPasswordGroupKey'];
			$password_groups[$item->PasswordGroupID] = $item;
		}
		return $password_groups;
	}

	public function delete() {
		if (!$this->PasswordGroupID) die ('no Password Group ID');
		if (PasswordGroups::is_populated($this->PasswordGroupID)) die('with this Password Group are still Passwords assotiated');
		$db    = new DB;
		$query = 'delete from PasswordGroups where PasswordGroupID=' . $this->PasswordGroupID;
		$db->execute($query);
	}

	public static function fetch_details_for_password_group_id($password_group_id) {
		if (!$password_group_id) die ('');
		$db    = new DB;
		$query = 'select * from PasswordGroups where PasswordGroupID=' . $password_group_id;

		$results = $db->query($query);
		$groups = array();
		foreach ($results as $result) {
			$item = new PasswordGroups;
			$item->PasswordGroupID                  = $result['PasswordGroupID'];
			$item->UserGroupID                      = $result['UserGroupID'];
			$item->PasswordGroupName                = $result['PasswordGroupName'];
			$item->AdminCryptedPasswordGroupKey     = unserialize(base64_decode($result['AdminCryptedPasswordGroupKey']));
			$item->UserGroupCryptedPasswordGroupKey = unserialize(base64_decode($result['UGroupCryptedPGroupKey']));
		}
		if (isset($item)) return $item;
	}

	public static function fetch_details_for_password_group_name($password_group_name) {
		if (!$password_group_name) die ('error no passwordgroup name');
		$db    = new DB;
		$query = 'select * from PasswordGroups where PasswordGroupName=\'' . $password_group_name . '\'';

		$groups = array();
		$results = $db->query($query);
		foreach ($results as $result) {
			$item = new PasswordGroups;
			$item->PasswordGroupID                  = $result['PasswordGroupID'];
			$item->UserGroupID                      = $result['UserGroupID'];
			$item->PasswordGroupName                = $result['PasswordGroupName'];
			$item->AdminCryptedPasswordGroupKey     = unserialize(base64_decode($result['AdminCryptedPasswordGroupKey']));
			$item->UserGroupCryptedPasswordGroupKey = unserialize(base64_decode($result['UGroupCryptedPGroupKey']));
		}
		if (isset($item)) {
			return $item;
		} else {
			return false;
		}
	}

	public function association_exists($group_id) {
		$db = new DB;
		$query =  'select count(*) from PasswordGroups where UserGroupID = ' . $group_id;
		$results =  $db->query($query);
		foreach ($results as $result) {
			if ($result[0] > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static function fetch_group_id_for_group_name($group_name) {
		$logger = new Logger;
		$db = new DB;
		$query = 'select PasswordGroupID from PasswordGroups where PasswordGroupName=\'' . $group_name . '\'';
		$results = $db->query($query);
		$logger->debug($query);
		foreach ($results as $result) {
			$group_id = $result['PasswordGroupID'];
		}
		if (isset($group_id)) {
			return $group_id;
		} else {
			return -1;
		}
	}

	private function is_populated($password_group_id){
		$db = new DB;
		$query =  'select count(*) from Passwords where PasswordGroupID = ' . $password_group_id;
		$results =  $db->query($query);
		foreach ($results as $result) {
			if ($result[0] > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

    public function create_table() {
        $db = new DB;
        $query = '
CREATE TABLE IF NOT EXISTS `' . DB . '`.`PasswordGroups` (
  `PasswordGroupID` INT(11) NOT NULL AUTO_INCREMENT,
  `UserGroupID` INT(11) NOT NULL,
  `PasswordGroupName` VARCHAR(50) NULL DEFAULT NULL,
  `AdminCryptedPasswordGroupKey` TEXT NULL DEFAULT NULL,
  `UGroupCryptedPGroupKey` TEXT NULL DEFAULT NULL,
  `private` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`PasswordGroupID`),
  INDEX `UserGroupID_idx` (`UserGroupID` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 61
DEFAULT CHARACTER SET = utf8
';
        $db->execute($query);

    }
}
