<?php
class Passwords {
	public $PasswordID;
	public $PasswordGroupID;
	public $Titel;
	public $UserName;
	public $Notes;
	public $PasswordGroupKeyCryptedPassword;
	public $Password;
	public $URL;
	public $AddTimestamp;
	public $AddUserID;
	public $EditTimestamp;
	public $EditUserID;
    public $idoitID;

	public function fetch_all_passwords($filter, $user_id){
		$logger = new Logger;

		$joined_password_group_ids = '';
		$password_groups           = PasswordGroups::fetch_all_groups($user_id, true);

		if (is_array($password_groups)) {
			foreach ($password_groups as $password_group) {
				$joined_password_group_ids .= $password_group->PasswordGroupID . ',';
			}
			$joined_password_group_ids = trim($joined_password_group_ids, ',');
		}

		$query = 'select Passwords.PasswordID, Passwords.UserName, Passwords.Notes, Passwords.Titel , Passwords.URL, Passwords.PasswordGroupID, PasswordGroups.PasswordGroupName from Passwords  join PasswordGroups on Passwords.PasswordGroupID=PasswordGroups.PasswordGroupID where 1 = 1 ';

        if (isset($filter['titel']) and $filter['titel'] != '')                         $query .= 'and (Titel like \'%' . $filter['titel'] . '%\' or UserName like \'%' . $filter['titel'] . '%\') ';
        if (isset($filter['password_group_id']) and $filter['password_group_id'] != '') $query .= 'and Passwords.PasswordGroupID=' . $filter['password_group_id'] . ' ';
        if ($user_id != ADMIN_UID && $joined_password_group_ids != '')                  $query .= 'and Passwords.PasswordGroupID in (' . $joined_password_group_ids . ') ';

        $query .= 'order by Titel ';
		if (isset($filter['page']) and $filter['page'] != '')                           $query .= 'limit 15 offset ' . 15 * ($filter['page'] - 1);

        $logger->debug($query);
        /*
        try {
        	$dsn = DB_SYSTEM . ':host=' . DB_HOST . ';dbname=' . DB;
        	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
        	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        	$passwords = $dbh->query($query);
        	$passwords->setFetchmode(PDO::FETCH_OBJ);
        	return $passwords;
        } catch (PDOException $e) {
        	echo 'Qery failed';
        }
        */
        $db    = new DB;

		$passwords = array();
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$item = new Passwords;
			$item->PasswordID                      = $result['PasswordID'];
			$item->PasswordGroupID                 = $result['PasswordGroupID'];
			$item->Titel                           = htmlentities($result['Titel']);
			$item->UserName                        = htmlentities($result['UserName']);
			$item->Notes                           = htmlentities($result['Notes']);
			$item->URL                             = htmlentities($result['URL']);

            if ($result['PasswordGroupName'] == $user_id) {
                $item->PasswordGroupName           = PERSONAL;
            } else {
                $item->PasswordGroupName           = $result['PasswordGroupName'];
            }
            
			//$item->PasswordGroupKeyCryptedPassword = unserialize(base64_decode($result['PwdGrpKeyCryptedPassword']));
			$passwords[] = $item;
		}
		return $passwords;
	}

    public function count($filter, $user_id) {
        $logger = new Logger;

        $joined_password_group_ids = '';
        $password_groups           = PasswordGroups::fetch_all_groups($user_id, true);

        if (is_array($password_groups)) {
            foreach ($password_groups as $password_group) {
                $joined_password_group_ids .= $password_group->PasswordGroupID . ',';
            }
            $joined_password_group_ids = trim($joined_password_group_ids, ',');
        }

        $query = 'select count(*) as count from Passwords where 1 = 1 ';

        if (isset($filter['titel']) and $filter['titel'] != '')                         $query .= 'and (Titel like \'%' . $filter['titel'] . '%\' or UserName like \'%' . $filter['titel'] . '%\') ';
        if (isset($filter['password_group_id']) and $filter['password_group_id'] != '') $query .= 'and PasswordGroupID=' . $filter['password_group_id'] . ' ';
        if ($user_id != ADMIN_UID && $joined_password_group_ids != '')                  $query .= 'and PasswordGroupID in (' . $joined_password_group_ids . ') ';

        $logger->debug($query);
        $db       = new DB;
        $results  = $db->query($query);
        foreach ($results as $result) {
            $count = $result['count'];
        }

        $pages = (int) (($count / 15) + 0.9);

        return $pages ;

    }

	public function delete_all_passowrds_from_passowrd_group($password_group_id){
		$db    = new DB;
		$query = 'delete from Passwords where PasswordGroupID=' . $password_group_id;
		$db->execute($query);
	}

	public static function fetch_details_for_password_id($password_id, $cookie_value){
		$crypto = new Crypto;
		$db     = new DB;
		$logger = new Logger;

		$query = 'select * from Passwords where PasswordID=' . $password_id;

		$results  = $db->query($query);
    	foreach ($results as $result) {
			$item = new Passwords;
			$item->PasswordID                      = $result['PasswordID'];
			$item->PasswordGroupID                 = $result['PasswordGroupID'];
			$item->Titel                           = $result['Titel'];
			$item->UserName                        = $result['UserName'];
			$item->Notes                           = $result['Notes'];
			$item->URL                             = $result['URL'];
			$item->PasswordGroupKeyCryptedPassword = unserialize(base64_decode($result['PwdGrpKeyCryptedPassword']));
			$item->AddTimestamp                    = $result['AddTimestamp'];
			$item->AddUserID                       = $result['AddUserID'];
			$item->EditTimestamp                   = $result['EditTimestamp'];
			$item->EditUserID                      = $result['EditUserID'];
            $item->idoitID                         = $result['idoitID'];
		}
		if (!$item->PasswordID) die ('Password does not exists');
		$logger->debug('*** Access attempt to Password ' . $item->Titel . '***');

		$users = new Users;
		$user            = $users->check_credentials_from_cookie($cookie_value);
		$userdetails     = Users::fetch_details_for_user_id($user->UserID);
		$userpassword    = Users::get_password_from_cookie($cookie_value);
		$crypted_userkey = unserialize(base64_decode($userdetails->UserCryptedUserKey));
		$joined_groups   = $userdetails->JoinedGroups;
		$password_group  = PasswordGroups::fetch_details_for_password_group_id($item->PasswordGroupID);

		if (isset($joined_groups[$password_group->UserGroupID])) $usergroup       = $joined_groups[$password_group->UserGroupID];





		if ($crypted_userkey && $userpassword) $userkey  = $crypto->decrypt($crypted_userkey, $userpassword);

		//var_dump($crypted_userkey);

		if (!isset($usergroup)) {
			$joined_group_ids = '';

			foreach ($joined_groups as $group_id => $group) {
				$joined_group_ids .= $group_id . ', ';
			}

			$item->Password = 'keine gemeinsame Gruppe';
			$logger->debug('user ' . $user->Name . ' has no common group with password group');
			$logger->debug('user\'s groups: ' . $joined_group_ids);
			if (isset($password_group)) $logger->debug('password\'s group: ' . $password_group->UserGroupID);

			$logger->access('user ' . $user->Name . ' has tried to access password ' . $item->Titel . ' but has no common user group with password');
		} else {
			$user_crypted_group_key = unserialize(base64_decode($usergroup['UserCryptedGroupKey']));
			$user_group_key         = $crypto->decrypt($user_crypted_group_key, $userkey);

			$usergroup_crypted_passwordgroup_key = $password_group->UserGroupCryptedPasswordGroupKey;
			$passwordgroup_key                   = $crypto->decrypt($usergroup_crypted_passwordgroup_key, $user_group_key);

			$logger->debug('password acces granted for usergroup ' . $usergroup['GroupName']);
			$logger->debug('user key: ' . $userkey);
			$logger->debug('password group key: ' . $passwordgroup_key);
			$logger->debug('user group key: ' . $user_group_key . "\n");

			$logger->access('user ' . $user->Name . ' has successfully accessed password ' . $item->Titel);

			$item->Password = $crypto->decrypt($item->PasswordGroupKeyCryptedPassword, $passwordgroup_key);
		}
		//$user_group = Groups::fetch_details_for_group($ugpgm->UserGroupID);

		return $item;
	}

	public function insert($cookie_value){
		$crypto = new Crypto;
		$users  = new Users;
		$groups = new Groups;
		$logger = new Logger;



		$loggedin_user    = $users->check_credentials_from_cookie($cookie_value);
		$crypted_user_key = unserialize(base64_decode($loggedin_user->UserCryptedUserKey));
		$user_password    = Users::get_password_from_cookie($cookie_value);
		$user_key         = $crypto->decrypt($crypted_user_key, $user_password);

		if ($loggedin_user->UserID == ADMIN_UID) { // wenn es der admin ist der das kennwort aendern will ist es einfach
			$password_group             = PasswordGroups::fetch_details_for_password_group_id($this->PasswordGroupID);
			if (!$password_group) die('passowrd group needed');
			$crypted_password_group_key = $password_group->AdminCryptedPasswordGroupKey;
			$password_group_key         = $crypto->decrypt($crypted_password_group_key, $user_key);




		} else { // alle anderen koennen passwoerter nur fuer ihre containergruppe anlegen 1.3.2010 oder besser doch alle wo Mitglied

			$userdetails                = Users::fetch_details_for_user_id($loggedin_user->UserID);
			$crypted_userkey            = unserialize(base64_decode($userdetails->UserCryptedUserKey));
			$joined_groups              = $userdetails->JoinedGroups;
			//$password_group             = PasswordGroups::fetch_details_for_password_group_name($loggedin_user->UserID);
			$password_group             = PasswordGroups::fetch_details_for_password_group_id($this->PasswordGroupID);
			$usergroup                  = $joined_groups[$password_group->UserGroupID];
			$crypted_password_group_key = $password_group->UserGroupCryptedPasswordGroupKey;
			$user_crypted_group_key     = unserialize(base64_decode($usergroup['UserCryptedGroupKey']));
			$user_group_key             = $crypto->decrypt($user_crypted_group_key, $user_key);
			$password_group_key         = $crypto->decrypt($crypted_password_group_key, $user_group_key);

			//$this->PasswordGroupID      = $password_group->PasswordGroupID;
		}


		$logger->debug('new password ' . $this->Titel . ' in passwordgroup ' . $password_group->PasswordGroupName);
		$logger->debug('password group key: ' . $password_group_key);
		if (isset($user_group_key)) $logger->debug('user group key: ' . $user_group_key);
		$logger->debug('user key: ' . $user_key);
		$password_group_key_crypted_password = base64_encode(serialize($crypto->encrypt($this->Password, $password_group_key)));

		$db    = new DB;
		$query = 'insert into Passwords (PasswordGroupID, Titel, UserName, Notes, URL, PwdGrpKeyCryptedPassword, AddTimestamp, idoitID, AddUserID) ' .
		          'values ' .
				  '(\'' . $this->PasswordGroupID . '\', ' .
					'\'' . $this->Titel . '\', ' .
					'\'' . $this->UserName . '\', ' .
					'\'' . $this->Notes . '\', ' .
					'\'' . $this->URL . '\', ' .
				    '\'' . $password_group_key_crypted_password . '\', ' .
					'\'' . time() . '\', ' .
					'\'' . $this->idoitID . '\', ' .
                    '\'' . $loggedin_user->UserID . '\')';

		$db->execute($query);

		$logger->access('user ' . $loggedin_user->Name . ' has added password ' . $this->Titel);
	}

	public function update($cookie_value) {
		$crypto = new Crypto;
		$users  = new Users;
		$groups = new Groups;
		$logger = new Logger;
	    $db     = new DB;

	    $query = 'select * from Passwords where PasswordID = ' . $this->PasswordID;
	    $results  = $db->query($query);
    	foreach ($results as $result) {
			$old_password_group_key_crypted_password = unserialize(base64_decode($result['PwdGrpKeyCryptedPassword']));
			$old_password_group_id                   = $result['PasswordGroupID'];
		}
		if (!$this->PasswordGroupID) {
			$this->PasswordGroupID = $old_password_group_id;
		}

		$loggedin_user      = $users->check_credentials_from_cookie($cookie_value);
		$crypted_user_key   = unserialize(base64_decode($loggedin_user->UserCryptedUserKey));
		$user_password      = Users::get_password_from_cookie($cookie_value);
		$user_key           = $crypto->decrypt($crypted_user_key, $user_password);
		$old_password_group = PasswordGroups::fetch_details_for_password_group_id($old_password_group_id);
		$new_password_group = PasswordGroups::fetch_details_for_password_group_id($this->PasswordGroupID);

		$logger->debug('old password group id: ' . $old_password_group_id);
		$logger->debug('new password group id: ' . $this->PasswordGroupID);

		if ($loggedin_user->UserID == ADMIN_UID) { // wenn es der admin ist der das kennwort aendern will ist es einfach
			$logger->access('admin user chanched details from password ' . $this->Titel);
			if (!$new_password_group) die('passowrd group needed');
			$old_crypted_password_group_key = $old_password_group->AdminCryptedPasswordGroupKey;
			$new_crypted_password_group_key = $new_password_group->AdminCryptedPasswordGroupKey;
			$old_password_group_key         = $crypto->decrypt($old_crypted_password_group_key, $user_key);
			$new_password_group_key         = $crypto->decrypt($new_crypted_password_group_key, $user_key);

		} else { //normale User muessen den Weg ueber die gemeinsame Usergroup gehen
			$logger->access('user ' . $loggedin_user->Name . ' chanched details from password ' . $this->Titel);
			$userdetails                = Users::fetch_details_for_user_id($loggedin_user->UserID);
			$crypted_userkey            = unserialize(base64_decode($userdetails->UserCryptedUserKey));
			$joined_groups              = $userdetails->JoinedGroups;

			if (!isset($joined_groups[$old_password_group->UserGroupID])) die('User and Password had no common Usergroup');
			if (!isset($joined_groups[$new_password_group->UserGroupID])) die('User and Password have no common Usergroup');

			$old_usergroup                  = $joined_groups[$old_password_group->UserGroupID];
			$new_usergroup                  = $joined_groups[$new_password_group->UserGroupID];
			$old_crypted_password_group_key = $old_password_group->UserGroupCryptedPasswordGroupKey;
			$new_crypted_password_group_key = $new_password_group->UserGroupCryptedPasswordGroupKey;

			$old_user_crypted_group_key     = unserialize(base64_decode($old_usergroup['UserCryptedGroupKey']));
			$new_user_crypted_group_key     = unserialize(base64_decode($new_usergroup['UserCryptedGroupKey']));
			$old_user_group_key             = $crypto->decrypt($old_user_crypted_group_key, $user_key);
			$new_user_group_key             = $crypto->decrypt($new_user_crypted_group_key, $user_key);

			$old_password_group_key         = $crypto->decrypt($old_crypted_password_group_key, $old_user_group_key);
			$new_password_group_key         = $crypto->decrypt($new_crypted_password_group_key, $new_user_group_key);

		}

		$old_password = $crypto->decrypt($old_password_group_key_crypted_password,$old_password_group_key);
		if ($this->Password == '') {
			$this->Password = $old_password;
		} else {
			$logger->access('and altered the encrypted password');
		}

		if ($new_password_group->PasswordGroupName != $old_password_group->PasswordGroupName) $logger->access('and altered the password group from ' . $old_password_group->PasswordGroupName . ' to ' . $new_password_group->PasswordGroupName);

		$logger->debug('update password ' . $this->Titel . ' in new passwordgroup ' . $new_password_group->PasswordGroupName);
		$logger->debug('old password: ' . $old_password);
		$logger->debug('new password: ' . $this->Password);
		$logger->debug('old password group key: ' . $old_password_group_key);
		$logger->debug('new password group key: ' . $new_password_group_key);
		if (isset($old_user_group_key)) $logger->debug('old user group key: ' . $old_user_group_key);
		if (isset($old_user_group_key)) $logger->debug('new user group key: ' . $new_user_group_key);
		$logger->debug('user key: ' . $user_key);


		$password_group_key_crypted_password = base64_encode(serialize($crypto->encrypt($this->Password, $new_password_group_key)));



		$query = 'update Passwords set ' .
			         'Titel = \'' .                     $this->Titel . '\', ' .
			         'UserName = \'' .                  $this->UserName . '\', ' .
			         'Notes = \'' .                     $this->Notes . '\', ' .
			         'URL = \'' .                       $this->URL . '\', ' .
			         'PwdGrpKeyCryptedPassword = \'' .  $password_group_key_crypted_password . '\', ' .
			         'PasswordGroupID = \'' .           $this->PasswordGroupID . '\', ' .
                     'idoitID = \'' .                   $this->idoitID . '\', ' .
			         'EditTimestamp = \'' .             time() . '\', ' .
			         'EditUserID = \'' .                $loggedin_user->UserID . '\' ' .
		         'where PasswordID = ' . $this->PasswordID;

		$db->execute($query);

		$logger->access('user ' . $loggedin_user->Name . ' has updated password ' . $this->Titel);
	}


	public function delete($cookie_value) {
		$logger = new Logger;
		$users  = new Users;
		$loggedin_user = $users->check_credentials_from_cookie($cookie_value);

		$db    = new DB;
		$query = 'delete from Passwords where PasswordID=' . $this->PasswordID;
		$db->execute($query);
		$logger->access('password ' . $this->Titel . ' deleted by user ' . $loggedin_user->Name);
	}

    public function create_table() {
        $db = new DB;
        $query = '
CREATE TABLE IF NOT EXISTS `' . DB . '`.`Passwords` (
  `PasswordID` INT(11) NOT NULL AUTO_INCREMENT,
  `PasswordGroupID` INT(11) NOT NULL,
  `Titel` VARCHAR(50) NULL DEFAULT NULL,
  `UserName` VARCHAR(50) NULL DEFAULT NULL,
  `Notes` VARCHAR(1024) NULL DEFAULT NULL,
  `PwdGrpKeyCryptedPassword` TEXT NULL DEFAULT NULL,
  `URL` VARCHAR(200) NULL DEFAULT NULL,
  `AddTimestamp` INT(11) NULL DEFAULT NULL,
  `AddUserID` INT(11) NULL DEFAULT NULL,
  `EditTimestamp` INT(11) NULL DEFAULT NULL,
  `EditUserID` INT(11) NULL DEFAULT NULL,
  `idoitID` INT(11) NOT NULL,
  PRIMARY KEY (`PasswordID`),
  INDEX `fk_Passwords_PasswordGroups1_idx` (`PasswordGroupID` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 1766
DEFAULT CHARACTER SET = utf8
';
        $db->execute($query);

    }
}
