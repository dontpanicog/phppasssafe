<?php
class UserGroup_PasswordGroup_Mapping {
	public $PasswordGroupID;
	public $UserGroupID;
	public $UserGroupCryptedPasswordGroupKey;

	public function fetch_usergroup__passwordgroup_mapping($passwordgroup_id, $usergroup_id){
		$db = new DB;
		$query = 'select * from UserGroup_PasswordGroup_Mapping where PasswordGroupID=' . $passwordgroup_id;

		$ugpgms = array();
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$ugpgm = new UserGroup_PasswordGroup_Mapping;
			$ugpgm->PasswordGroupID                  = $result['PasswordGroupID'];
			$ugpgm->UserGroupID                      = $result['UserGroupID'];
			$ugpgm->UserGroupCryptedPasswordGroupKey = $result['UGCryptedPasswordGKey'];
			$ugpgms[$result['UserGroupID']] = $ugpgm;
		}
		return $ugpgms;
	}

	public function association_exists($user_group_id){
		$db = new DB;
		$query =  'select count(*) from UserGroup_PasswordGroup_Mapping where UserGroupID = ' . $user_group_id;
		$results =  $db->query($query);
		foreach ($results as $result) {
			if ($result[0] > 0) {
				return true;
			} else {
				return false;
			}
		}
	}
}
