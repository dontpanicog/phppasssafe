<?php

class Users {
	public $UserID;
	public $Name;
	public $FirstName;
	public $LastName;
	public $email;
	public $submitted_by;
	public $submitted_on;
	public $altered_by;
    public $altered_on;
    public $SessionData;
    public $SessionKey;
    public $SessionTimeout;
    public $UserPassword;
    public $active;
    public $HashedPassword;
    public $JoinedGroups;
    public $AvailableGroups;
    public $UserKey;
    public $Salt;

    public function check_credentials_from_login($name, $password) {
    	$logger = new Logger;
		$crypto = new Crypto;
    	$db     = new DB;
    	$query  = 'select * from Users where Name=\'' . $name . '\' and active=1';
    	$logger->debug($query);
    	$results  = $db->query($query);
    	foreach ($results as $result) {
			$item                     = new Users;
			$item->UserID             = $result['UserID'];
			$item->Name               = $result['Name'];
			$item->FirstName          = $result['FirstName'];
			$item->LastName           = $result['LastName'];
			$item->email              = $result['email'];
			$item->active             = $result['active'];
			$item->Salt               = $result['Salt'];
			$item->HashedPassword     = $result['HashedPassword'];
			//$item->UserPassword       = $password;
		}

		$item->SessionTimeout = time()+INACTIVITYTIMEOUT;
		$sessionkey           = $crypto->generate_key('20');
		$item->SessionData    = $crypto->encrypt($password, $sessionkey);
		
		if (isset($item->Salt) && $item->Salt != '') {
			$hashed_password = Crypto::calculate_hashed_password($item->Salt, 500,  $password);
		} else {
			$hashed_password = md5($password);
		}
		

		if (isset($item->HashedPassword) && ($hashed_password == $item->HashedPassword)) {
			$query = 'update Users set SessionTimeout=\'' . $item->SessionTimeout . '\', ' .
			                          'SessionKey=\'' . $sessionkey . '\' ' .
					 'where UserID=\'' . $item->UserID . '\'';

			$db->execute($query);

			$cookie_data = base64_encode(serialize($item));
			setcookie("phpPassSafe", $cookie_data, $item->SessionTimeout, '', '', 0);
			$logger->access('user ' . $item->Name . ' logged in');
			return true;
		} else {
			$logger->access('failed login attempt with user name ' . $name);
			if (isset($item->HashedPassword)) $logger->debug('stored hash: ' . $item->HashedPassword);
			
			$logger->debug('Calculated Hash from submitted Password ' . $password . ' and stored Salt ' . $item->Salt . ': ' . $hashed_password);
			return false;
		}

    }

    public function check_credentials_from_cookie($cookie_value) {
    	$crypto = new Crypto;
    	$user   = new Users;
    	$logger = new Logger;

    	$user = unserialize(base64_decode($cookie_value));
    	$logger->debug('checking credentials for user ' . $user->Name);
    	if ($user->UserID) {
	    	$db    = new DB;
	    	$query = 'select * from Users where UserID=\'' . $user->UserID . '\'';
	    	$results  = $db->query($query);
    		foreach ($results as $result) {
				$user->HashedPassword      = $result['HashedPassword'];
				$user->SessionKey          = $result['SessionKey'];
				$user->SessionTimeout      = $result['SessionTimeout'];
				$user->UserCryptedUserKey  = $result['UserCryptedUserKey'];
				$user->AdminCryptedUserKey = $result['AdminCryptedUserKey'];
				$user->Salt                = $result['Salt'];
			}


	    	$password = $crypto->decrypt($user->SessionData, $user->SessionKey);


	    	if (isset($user->Salt) && $user->Salt != '') {
	    		$hashed_password = Crypto::calculate_hashed_password($user->Salt,  500, $password);
	    	} else {
	    		$hashed_password = md5($password);
	    	}

	    	if (($hashed_password == $user->HashedPassword && $user->SessionTimeout > time())) {
	    		$user->SessionTimeout = time()+INACTIVITYTIMEOUT;
				$query = 'update Users set SessionTimeout=\'' . $user->SessionTimeout . '\' ' .
						 'where UserID=\'' . $user->UserID . '\'';

				$db->execute($query);
				$logger->debug('access granted for user ' . $user->Name);
				$cookie_data = base64_encode(serialize($user));
				setcookie("phpPassSafe", $cookie_data, $user->SessionTimeout, '', '', 0);
				return $user;
			} else {
				return false;
			}
    	} else {
    		return false;
    	}
    }

    public static function get_password_from_cookie($cookie_value) {
    	$crypto = new Crypto;
    	$user = new Users;

    	$user = unserialize(base64_decode($cookie_value));

    	$db    = new DB;
    	$query = 'select * from Users where UserID=\'' . $user->UserID . '\'';
    	$results  = $db->query($query);
    	foreach ($results as $result) {
			$user->HashedPassword = $result['HashedPassword'];
			$user->SessionKey     = $result['SessionKey'];
			$user->SessionTimeout = $result['SessionTimeout'];
		}


    	$password = $crypto->decrypt($user->SessionData, $user->SessionKey);
    	return $password;
    }

	public function fetch_all_users($filter){
		$db    = new DB;
		$query = 'select * from Users ';
		if ($filter['group_id']  != '') {
			$query .= 'INNER JOIN Users_Groups_Mapping ON Users.UserID = Users_Groups_Mapping.UserID where GroupID =  ' . $filter['group_id'] . ' ';
		} else {
			$query .= 'where 1 = 1 ';
		}
        if ($filter['name']      != '') $query .= 'and Name like \'%' . $filter['name'] . '%\' ';
        if ($filter['firstname'] != '') $query .= 'and FirstName like \'%' . $filter['firstname'] . '%\' ';
        if ($filter['lastname']  != '') $query .= 'and LastName like \'%' . $filter['lastname'] . '%\' ';
        $query .= 'order by ' . $filter['order_by'] . ' ' . $filter['order'];
        /*
        try {
        	$dsn = DB_SYSTEM . ':host=' . DB_HOST . ';dbname=' . DB;
        	$dbh = new PDO($dsn, DB_USER, DB_PASSWORD);
        	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        	$users = $dbh->query($query);
        	$users->setFetchmode(PDO::FETCH_OBJ);
        	return $users;
        } catch (PDOException $e) {
        	echo 'Qery failed: ' . $e->getMessage();
        }
        */
        $users = array();
        $results  = $db->query($query);
    	foreach ($results as $result) {
        	$user = new Users;
        	$user->UserID             = $result['UserID'];
        	$user->Name               = $result['Name'];
        	$user->FirstName          = $result['FirstName'];
        	$user->LastName           = $result['LastName'];
			$user->email              = $result['email'];
			$user->active             = $result['active'];
			$user->SessionTimeout     = $result['SessionTimeout'];
			$user->HashedPassword     = $result['HashedPassword'];
			$user->UserCryptedUserKey = $result['UserCryptedUserKey'];
			$users[] = $user;
        }
        return $users;
	}

	public static function fetch_details_for_user_id($user_id){
		$crypto = new Crypto;
		$db     = new DB;

		$query  = 'select * from Users where UserID=\'' . $user_id . '\'';
		$results  = $db->query($query);
		$item                     = new Users;
    	foreach ($results as $result) {
			
			$item->UserID             = $result['UserID'];
			$item->Name               = $result['Name'];
			$item->FirstName          = $result['FirstName'];
			$item->LastName           = $result['LastName'];
			$item->email              = $result['email'];
			$item->active             = $result['active'] ? 'checked="checked"' : '';
			$item->SessionTimeout     = $result['SessionTimeout'];
			$item->HashedPassword     = $result['HashedPassword'];
			$item->UserCryptedUserKey = $result['UserCryptedUserKey'];
		}

		$joined_groups = array();
		$query = 'SELECT     ugm.UserID, ugm.GroupID, ugm.UserCryptedGroupKey, g.GroupID AS Expr1, g.GroupName, g.UserContainer ' .
		         'FROM Users_Groups_Mapping AS ugm INNER JOIN Groups AS g ON ugm.GroupID = g.GroupID ' .
		         'where ugm.UserID=\'' . $user_id . '\'';
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$joined_groups[$result['GroupID']] = $result;
		}
		$item->JoinedGroups = $joined_groups;

		$available_groups = array();
		$query = 'select * from Groups where UserContainer=NULL or UserContainer=0 order by GroupName asc';

		$available_groups = array();
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$available_groups[$result['GroupID']] = $result;
		}

		$available_groups = array_diff_key($available_groups, $joined_groups);

		$item->AvailableGroups = $available_groups;
		return $item;
	}

	public function fetch_user_crypted_user_key_for_user_id($user_id) {
		$db     = new DB;
		$user_crypted_user_key = array();
		$query  = 'select * from Users where UserID=\'' . $user_id . '\'';
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$storeable_user_crypted_user_key  = $result['UserCryptedUserKey'];
			$serialized_user_crypted_user_key = base64_decode($storeable_user_crypted_user_key);
			$user_crypted_user_key            = unserialize($serialized_user_crypted_user_key);
		}
		return $user_crypted_user_key;
	}

	public function fetch_admin_crypted_user_key_for_user_id($user_id) {
		$db     = new DB;

		$query  = 'select * from Users where UserID=\'' . $user_id . '\'';
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$storeable_admin_crypted_user_key  = $result['AdminCryptedUserKey'];
			$serialized_admin_crypted_user_key = base64_decode($storeable_admin_crypted_user_key);
			$admin_crypted_user_key            = unserialize($serialized_admin_crypted_user_key);
		}
		return $admin_crypted_user_key;
	}

	public function update($cookie_value) {
		$logger = new Logger;
		$db     = new DB;
		$crypto = new Crypto;

		$logged_in_user = new Users;
    	$logged_in_user = unserialize(base64_decode($cookie_value));

		if ($this->UserPassword != '') { //wenn das Passwort geaendert wird, muss auch der UserCryptedUserKey neu verschl. werden
			if ($logged_in_user->UserID == ADMIN_UID) {
				$logger->debug('updating User Password to ' . $this->UserPassword . ' from user : ' . $this->Name. ' initialised by admin');
				$admin_crypted_user_key = Users::fetch_admin_crypted_user_key_for_user_id($this->UserID);
				$admin_password         = Users::get_password_from_cookie($cookie_value);

				$crypted_admin_key      = Users::fetch_user_crypted_user_key_for_user_id(ADMIN_UID);
				$admin_key              = $crypto->decrypt($crypted_admin_key, $admin_password);

				$user_key               = $crypto->decrypt($admin_crypted_user_key, $admin_key);
				
				$this->Salt = Crypto::generate_key(100);
				$this->HashedPassword = Crypto::calculate_hashed_password($this->Salt,  500, $this->UserPassword);
				
				$logger->debug('Generated Salt: ' . $this->Salt);
				$logger->debug('HashedPassword: ' . $this->HashedPassword);

				$logger->debug('user key: ' . $user_key);

				$user_crypted_user_key  = base64_encode(serialize($crypto->encrypt($user_key, $this->UserPassword)));

				$query = 'update Users set ' .
							'Name=\'' .               $this->Name . '\', ' .
							'FirstName=\'' .          $this->FirstName . '\', ' .
							'LastName=\'' .           $this->LastName . '\', ' .
							'email=\'' .              $this->email . '\', ' .
							'Salt=\'' .               $this->Salt . '\', ' .
							'HashedPassword=\'' .     $this->HashedPassword . '\', ' .
				            'active=\'' .             $this->active . '\', ' .
							'UserCryptedUserKey=\'' . $user_crypted_user_key . '\' ' .
						 'where UserID=\'' .  $this->UserID . '\'';
			} else {
				$logger->debug('updating User Password to ' . $this->UserPassword . ' from user : ' . $this->Name. ' initialised by user him self');

				$this->Salt = Crypto::generate_key(100);
				$this->HashedPassword = Crypto::calculate_hashed_password($this->Salt, 500, $this->UserPassword);

				$user_password         = Users::get_password_from_cookie($cookie_value);

				$crypted_user_key      = Users::fetch_user_crypted_user_key_for_user_id($logged_in_user->UserID);
				$user_key              = $crypto->decrypt($crypted_user_key, $user_password);


				$logger->debug('user key: ' . $user_key);

				$user_crypted_user_key  = base64_encode(serialize($crypto->encrypt($user_key, $this->UserPassword)));

				$query = 'update Users set ' .
							'Name=\'' .               $this->Name . '\', ' .
							'FirstName=\'' .          $this->FirstName . '\', ' .
							'LastName=\'' .           $this->LastName . '\', ' .
							'email=\'' .              $this->email . '\', ' .
							'HashedPassword=\'' .     $this->HashedPassword . '\', ' .
							'Salt=\'' .               $this->Salt . '\', ' .
				            'active=\'' .             $this->active . '\', ' .
							'UserCryptedUserKey=\'' . $user_crypted_user_key . '\' ' .
						 'where UserID=\'' .  $this->UserID . '\'';
			}
		} else {
			$query = 'update Users set ' .
						'Name=\'' .       $this->Name . '\', ' .
						'FirstName=\'' .  $this->FirstName . '\', ' .
						'LastName=\'' .   $this->LastName . '\', ' .
						'email=\'' .      $this->email . '\', ' .
			            'active=\'' .     $this->active . '\' ' .
					 'where UserID=\'' .  $this->UserID . '\'';
		}
		$logger->debug($query);
		$db->execute($query);
	}

	public function logout(){
		$logger = new Logger;
		$db     = new DB;
		$logger->access('user ' . $this->Name . ' logged out');
		$session_timeout = time()-600;
		$sessionkey      = '';
		$sessiondata     = '';


		$query = 'update Users set SessionTimeout=\'' . $session_timeout . '\', ' .
		                          'SessionKey=\'' . $sessionkey . '\' ' .
				 'where UserID=\'' . $this->UserID . '\'';

		$db->execute($query);
		setcookie("phpPassSafe", '', -9999, '', '', 0);
	}

	public function insert($cookie_value) {
		$crypto = new Crypto;
		$this->Salt = Crypto::generate_key(100);
		$this->HashedPassword = Crypto::calculate_hashed_password($this->Salt, 500, $this->UserPassword);

		$user_key               = $crypto->generate_key(20);

		$user_crypted_user_key  = base64_encode(serialize($crypto->encrypt($user_key, $this->UserPassword)));

		$admin_password         = Users::get_password_from_cookie($cookie_value);
		$crypted_admin_key      = Users::fetch_user_crypted_user_key_for_user_id(ADMIN_UID);
		$admin_key              = $crypto->decrypt($crypted_admin_key, $admin_password);

		$admin_crypted_user_key = base64_encode(serialize($crypto->encrypt($user_key, $admin_key)));

		$db    = new DB;
		$query = 'insert into Users (Name, FirstName, LastName, email, active, HashedPassword, Salt, UserCryptedUserKey, AdminCryptedUserKey) ' .
		         'VALUES (' .
		         	'\'' . $this->Name . '\', ' .
		         	'\'' . $this->FirstName . '\', ' .
		         	'\'' . $this->LastName . '\', ' .
		         	'\'' . $this->email . '\', ' .
		         	'\'' . $this->active . '\', ' .
		         	'\'' . $this->HashedPassword . '\', ' .
		         	'\'' . $this->Salt . '\', ' .
                    '\'' . $user_crypted_user_key . '\', ' .
                    '\'' . $admin_crypted_user_key . '\'' .
	         	')';

		$db->execute($query);

		$user_id = Users::fetch_user_id_for_user_name($this->Name);

		$container_group = new Groups;
		$container_group->GroupName = $user_id;
		$container_group->UserContainer = 1;
		$container_group->add_group($cookie_value);

		$group_id = Groups::fetch_group_id_for_group_name($user_id);
		Users::add_user_to_group($user_id, $group_id, $cookie_value);

		// Und jetzt noch eine Passwortgruppe fuer die privaten Passwoerter anlegen
		$private_password_group = new PasswordGroups;

		$private_password_group->UserGroupID       = $group_id;
		$private_password_group->PasswordGroupName = $user_id;
		$private_password_group->private           = true;
		$private_password_group->add_group($cookie_value);
	}

	public function add_user_to_group($user_id, $group_id, $cookie_value) {
		$crypto = new Crypto;
		$groups = new Groups;
		$logger = new Logger;

		$logger->debug('adding user ID ' . $user_id . 'user group ' . $group_id);

		$selected_group          = $groups->fetch_details_for_group($group_id);
		$admin_crypted_group_key = $selected_group->AdminCryptedGroupKey;
		$admin_crypted_user_key  = Users::fetch_admin_crypted_user_key_for_user_id($user_id);
		$admin_password          = Users::get_password_from_cookie($cookie_value);
		$crypted_admin_key       = Users::fetch_user_crypted_user_key_for_user_id(ADMIN_UID);
		$admin_key               = $crypto->decrypt($crypted_admin_key, $admin_password);
		$group_key               = $crypto->decrypt($admin_crypted_group_key, $admin_key);
		$user_key                = $crypto->decrypt($admin_crypted_user_key, $admin_key);

		$logger->debug('user key ' . $user_key);
		$logger->debug('group key ' . $group_key . "\n");

		$user_crypted_group_key  = base64_encode(serialize($crypto->encrypt($group_key, $user_key)));



		$db    = new DB;

		$query = 'insert into Users_Groups_Mapping (UserID, GroupID, UserCryptedGroupKey) ' .
		         'VALUES (\'' . $user_id . '\', \'' . $group_id . '\', \'' . $user_crypted_group_key . '\')';

		$db->execute($query);
	}

	public function del_user_from_group($user_id, $group_id) {
		$db    = new DB;
		$query = 'delete from Users_Groups_Mapping where UserID=' . $user_id . ' and GroupID=' . $group_id;

		$db->execute($query);
	}

	public function delete() {
		$logger = new Logger;
		if ($this->UserID == ADMIN_UID) die ('never delete the admin');
		$db    = new DB;
		$logger->debug('Deleting User ' . $this->UserID);
		// Dann die private Passwort Gruppe und alle damit gemapten Passwoerter loeschen
		$private_password_group_id = PasswordGroups::fetch_group_id_for_group_name($this->UserID);
		$logger->debug('Deleting all Passwords from Group ' . $private_password_group_id);

		Passwords::delete_all_passowrds_from_passowrd_group($private_password_group_id);
		$private_password_group= new PasswordGroups;
		$private_password_group->PasswordGroupID = $private_password_group_id;

		$logger->debug('Deleting all Passwords Group ' . $private_password_group_id);
		$private_password_group->delete();

		// Erst die Container Gruppe finden und diese loeschen
		$container_group_id = Groups::fetch_group_id_for_group_name($this->UserID);
		// Den User aus seiner Container Gruppe entfernen
		Users::del_user_from_group($this->UserID, $container_group_id);

		$container_group = new Groups;
		$container_group->GroupID = $container_group_id;
		$logger->debug('Deleting all User Container Group ' . $container_group_id);
		$container_group->delete();

		// Erst jetzt die den User und seine Mappings zu Gruppen loeschen
		$query = 'delete from Users where UserID=' . $this->UserID;
		$db->execute($query);
		$query = 'delete from Users_Groups_Mapping where UserID=' . $this->UserID;
		$db->execute($query);
	}

	public function fetch_joined_groups($user_id) {

	}

	public function fetch_user_id_for_user_name($user_name){
		$db    = new DB;
		$query = 'select UserID from Users where Name = \'' . $this->Name . '\'';
		$results  = $db->query($query);
    	foreach ($results as $result) {
			$user_id = $result['UserID'];
		}
		if ($user_id) {
			return $user_id;
		} else {
			return -1;
		}
	}

	public static function admin_exists() {
		$db    = new DB;
		$query = 'select * from Users where UserID = ' . ADMIN_UID;
		$results = array();
		$user_id = false; 
		$results = $db->query($query);
    	foreach ($results as $result) {
			$user_id = $result['UserID'];
		}
		if ($user_id) {
			return true;
		} else {
			return false;
		}
	}

	public function create_admin(){
		$crypto = new Crypto;
		$logger = new Logger;
		$logger->debug('Creating admin account');
		$user_key             = $crypto->generate_key(20);
		$password_crypted_key = base64_encode(serialize($crypto->ssl_encrypt($user_key, $this->UserPassword)));
		$key_crypted_key      =	base64_encode(serialize($crypto->ssl_encrypt($user_key, $user_key)));
		$salt                 = $crypto->generate_key(20);


		$db    = new DB;
		$query = 'insert into Users (UserID, Name, FirstName, LastName, active, HashedPassword, Salt, UserCryptedUserKey, AdminCryptedUserKey) ' .
		         'VALUES (' .
                    '\'' . ADMIN_UID . '\', ' .
		         	'\'' . $this->Name . '\', ' .
		         	'\'' . $this->FirstName . '\', ' .
		         	'\'' . $this->LastName . '\', ' .
		         	'\'' . $this->active . '\', ' .
		         	'\'' . $crypto->calculate_hashed_password($salt, 500, $this->UserPassword) . '\', ' .
                    '\'' . $salt . '\', ' .
                    '\'' . $password_crypted_key . '\', ' .
                    '\'' . $key_crypted_key . '\'' .
	         	')';

		$db->execute($query);
		$logger->debug($query);
		$logger->debug('verifying if account was created successfully');
		$query = 'select * from Users where Name=\'' . $this->Name . '\'';
		$logger->debug($query);
		$results = $db->query($query);
		foreach ($results as $result) {
		    $salt = $result['Salt'];
			if ($result['HashedPassword'] == $crypto->calculate_hashed_password($salt, 500, $this->UserPassword)) {
				$logger->debug('everything is ok');
				return $result['UserID'];
			} else {

				$logger->debug('something went wrong: ' . $result['HashedPassword'] . ' vs. ' . $crypto->calculate_hashed_password($salt, 500, $this->UserPassword));
				return false;
			}
		}
	}

    public function obj_count($filter) {

	    $users = $this->fetch_all_users($filter);
        $count = count($users);

        $pages = (int) (($count / 15) + 0.9);

        return $pages ;

    }


    public function create_table() {
        $db = new DB;
        $query = '
CREATE TABLE IF NOT EXISTS `' . DB . '`.`Users` (
  `UserID` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(50) NULL DEFAULT NULL,
  `FirstName` VARCHAR(50) NULL DEFAULT NULL,
  `LastName` VARCHAR(50) NULL DEFAULT NULL,
  `email` VARCHAR(50) NULL DEFAULT NULL,
  `submitted_on` DATE NULL DEFAULT NULL,
  `submitted_by` INT(11) NULL DEFAULT NULL,
  `altered_on` DATE NULL DEFAULT NULL,
  `altered_by` INT(11) NULL DEFAULT NULL,
  `active` TINYINT(1) NULL DEFAULT NULL,
  `SessionKey` VARCHAR(20) NULL DEFAULT NULL,
  `SessionData` TEXT NULL DEFAULT NULL,
  `SessionTimeout` INT(11) NULL DEFAULT NULL,
  `HashedPassword` VARCHAR(50) NULL DEFAULT NULL,
  `UserCryptedUserKey` TEXT NULL DEFAULT NULL,
  `AdminCryptedUserKey` TEXT NULL DEFAULT NULL,
  `Salt` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 62
DEFAULT CHARACTER SET = utf8
';
        $db->execute($query);

    }

}
