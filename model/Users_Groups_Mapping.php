<?php
class Users_Groups_Mapping {
	public $UserID;
	public $GroupID;
	public $UserCryptedGroupKey;
	
	public function fetch_users_groups_mapping($user_id, $group_id) {
	}

    public function create_table() {
        $db = new DB;
        $query = '
CREATE TABLE IF NOT EXISTS `' . DB . '`.`Users_Groups_Mapping` (
  `UserID` INT(11) NOT NULL,
  `GroupID` INT(11) NOT NULL,
  `UserCryptedGroupKey` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`UserID`, `GroupID`),
  INDEX `fk_Users_Groups_Mapping_Groups1_idx` (`GroupID` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
';
        $db->execute($query);

    }
}
