<div class="modal fade" role="dialog" tabindex="-1" id="RealyDeleteDiag">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-danger" role="alert"><span><strong>{$confirm_delete}</strong></span></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" data-dismiss="modal">{$cancel} </button>
                <button class="btn btn-danger" type="button" id="RealyDeletePassword" data-dismiss="modal">{$delete} </button>
            </div>
        </div>
    </div>
</div>
