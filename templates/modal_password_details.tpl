<div class="modal fade" role="dialog" tabindex="-1" id="PasswordDetails">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form name="PasswordDetailsForm" id="PasswordDetailsForm">
                    <input type="hidden" id="PasswordID">
                    <div class="form-group has-feedback">
                        <label class="control-label">Bezeichnung </label>
                        <input class="form-control" type="text" id="Titel"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label">Anwendername </label>
                        <input class="form-control" type="text" id="UserName"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div class="form-group has-feedback">
                        <lable class="control-label">Kennwort alternativ</lable>

                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </div>
                            <input class="form-control" type="password" id="Password" />
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-1" role="button" href="#collapse-1" onclick="GeneratePassword();" title="{$display_password_generator}"> <i class="glyphicon glyphicon-option-horizontal"></i></button>
                            </div>
                        </div>

                    </div>


                    <div><a class="btn btn-default hidden" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-1" role="button" href="#collapse-1">Show Content</a>
                        <div class="collapse" id="collapse-1">
                            <div class="well">
                                <form id="password-generator">
                                    <input type="hidden" value="{$generated_password_prefix}" name="prefix" id="prefix">
                                    <div class="form-group">
                                        <label class="control-label">{$length} </label>

                                        <select class="form-control" name="password-length" id="password-length" title="{$length}" onchange="GeneratePassword();">
                                            {for $option=1 to 40}
                                                <option {if $default_length == $option} selected="selected" {/if}>{$option}</option>
                                            {/for}
                                        </select>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="lower-case-characters" id="lower-case-characters" {if $preselect_lowercase} checked="checked" {/if} onchange="GeneratePassword();">{$lower_case}</label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="upper-case-characters" id="upper-case-characters" {if $preselect_uppercase} checked="checked" {/if} onchange="GeneratePassword();">{$upper_case}</label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="digit-characters" id="digit-characters" {if $preselect_digits} checked="checked" {/if} onchange="GeneratePassword();">{$digits}</label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="special-characters" id="special-characters" {if $preselect_specialcharacters} checked="checked" {/if} onchange="GeneratePassword();">{$special_characters}</label>
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="password-suggestion" id="password-suggestion">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" name="generate" id="generate"><i class="glyphicon glyphicon-repeat" onclick="GeneratePassword();"></i></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapse-1" href="#collapse-1" id="accept-suggestion">übernehmen </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">{$group} </label>
                        <select class="form-control" id="PasswordGroupID">
                            {foreach from=$password_groups item="password_group"}
                                {if $password_group->PasswordGroupName == $user_id}
                                    <option value="{$password_group->PasswordGroupID}">{$personel}</option>
                                {else}
                                    <option value="{$password_group->PasswordGroupID}">{$password_group->PasswordGroupName}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label">URL </label>
                        <input class="form-control" type="text" id="URL"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                        <div class="url">
                            <a href="" id="URL1" target="_blank"></a>
                            <span id="URLedit"><i class="glyphicon glyphicon-edit"></i></span>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label">Notizen </label>
                        <textarea class="form-control" id="Notes"></textarea><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>
                    <div class="form-group">
                        <label class="control-label">i-doit Objekt </label>
                        <select  class="form-control chosen-select" tabindex="2" id="IdoitObject">
                            <option></option>
                            {foreach from=$idoit_objs item="idoit_obj"}
                                <option value="{$idoit_obj->id}">{$idoit_obj->title}</option>
                            {/foreach}
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn-sm btn-default btn-close" type="button" data-dismiss="modal">{$close}</button>
                <button class="btn-sm btn-danger" type="button" id="DeletePassword" data-dismiss="modal" data-target="#RealyDeleteDiag" data-toggle="modal">{$delete} </button>
                <button class="btn-sm btn-primary" type="button" id="SavePassword">{$save}</button>
            </div>
        </div>
    </div>
</div>
