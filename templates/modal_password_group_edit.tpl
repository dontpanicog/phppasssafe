<div class="modal fade" role="dialog" tabindex="-1" id="PasswordGroupDetails">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form name="PasswordDetailsForm" id="PasswordGroupDetailsForm">
                    <input type="hidden" id="FormPasswordGroupID">
                    <div class="form-group has-feedback">
                        <label class="control-label">Bezeichnung </label>
                        <input class="form-control" type="text" id="GroupTitel"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div class="form-group">
                        <label class="control-label">{$group} </label>
                        <select class="form-control" id="UserGroupID">
                            {foreach from=$user_groups item="user_group"}
                                <option value="{$user_group->GroupID}">{$user_group->GroupName}</option>
                            {/foreach}
                        </select>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button class="btn-sm btn-default" type="button" data-dismiss="modal">{$close}</button>
                <button class="btn-sm btn-danger" type="button" id="DeletePasswordGroup" data-dismiss="modal" data-target="#RealyDeleteDiag" data-toggle="modal">{$delete} </button>
                <button class="btn-sm btn-primary" type="button" id="SavePasswordGroup">{$save}</button>
            </div>
        </div>
    </div>
</div>
