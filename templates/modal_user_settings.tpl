<div class="modal fade" role="dialog" tabindex="-1" id="UserSettings">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form name="UserSettingsForm" id="UserSettingsForm">
                    <input type="hidden" id="UserID" value="{$loggedin_user->UserID}">

                    <div id="fg_first_name" class="form-group has-feedback">
                        <label class="control-label">{$first_name} </label>
                        <input class="form-control" type="text" id="first_name" name="first_name" value="{$loggedin_user->FirstName}"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div id="fg_last_name" class="form-group has-feedback">
                        <label class="control-label">{$last_name} </label>
                        <input class="form-control" type="text" id="last_name" name="last_name" value="{$loggedin_user->LastName}"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div id="fg_user_name" class="form-group has-feedback">
                        <label class="control-label">{$user_name} </label>
                        <input class="form-control" type="text" id="user_name" name="user_name" value="{$loggedin_user->Name}"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div id="fg_email_name" class="form-group has-feedback">
                        <label class="control-label">{$email} </label>
                        <input class="form-control" type="email" id="email" name="email" value="{$loggedin_user->email}"><i class="form-control-feedback glyphicon glyphicon-exclamation-sign hidden" aria-hidden="true"></i>
                    </div>

                    <div id="fg_UserPassword" class="form-group has-feedback Password">
                        <label class="control-label">{$password} </label>
                        <input class="form-control" type="password" id="UserPassword">
                    </div>

                    <div id="fg_RepeatUserPassword" class="form-group has-feedback Password">
                        <label class="control-label">{$repeat_password} </label>
                        <input class="form-control" type="password" id="RepeatUserPassword">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" data-dismiss="modal">{$close}</button>
                <button class="btn btn-primary" type="button" id="SaveUserSettings">{$save}</button>
            </div>
        </div>
    </div>
</div>
